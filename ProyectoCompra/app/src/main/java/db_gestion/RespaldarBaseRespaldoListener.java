package db_gestion;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class RespaldarBaseRespaldoListener extends AsyncTask<String, Void, String> {
    Context contexto;
    private ProgressDialog progressDialog;
    boolean exito;
    public RespaldarBaseRespaldoListener(Context contexto){
        this.contexto = contexto;
    }
    @Override
    protected String doInBackground(String... params) {
        exito=RespaldarBaseDatos(contexto);
        return null;
    }
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        progressDialog.dismiss();
        alerta(exito);
    }
    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        progressDialog = ProgressDialog.show(contexto, "", "Respaldando la base de datos..", true);

    }

    public boolean RespaldarBaseDatos(Context context){

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy h:mm:ss a", Locale.ENGLISH);
        String currentDateandTime = sdf.format(new Date());
        // Creo el directoio para guardar el fichero
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/RESPALDOS_DB/");

        // si el direcctorio no existe, lo creo
        if (!dir.exists()) {
            if(dir.mkdir()){
                Log.e("Respaldo de base", "Si se creo el directorio");
            }else{
                Log.e("Respaldo de base", "El directorio no fue creado");
            }
        }
        //Creamos la ruta actual de la base de datos
        String currentDBPath = "//data//com.fichafamiliar//databases//fichaFamAndroid.db";
        //Creamos el archivo que contiene la base de datos actual
        File data = Environment.getDataDirectory();
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(dir,currentDateandTime+"_fichaFamAndroid.db");
        try {
            if (currentDB.exists()) {
                @SuppressWarnings("resource")
                FileChannel recursos = new FileInputStream(currentDB).getChannel();
                @SuppressWarnings("resource")
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(recursos, 0, recursos.size());
                recursos.close();
                dst.close();
                return true;
            }else{
                Log.e("Error en respaldo", "La base de datos no existe");
                return false;
            }
        } catch (Exception e) {
            Log.e("Error en respaldo", "Ocurrio un exepcion: "+e.getMessage());
            return false;
        }
    }
    public void alerta(boolean exito){
        if(exito){
            Toast.makeText(contexto,"El respaldo se ha generado con exito",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(contexto,"Error al interntar hacer el respaldo",Toast.LENGTH_SHORT).show();
        }
     }
}
