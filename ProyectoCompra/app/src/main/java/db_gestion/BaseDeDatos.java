package db_gestion;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BaseDeDatos extends SQLiteOpenHelper {
	private static String DB_PATH = "/data/data/com.moy.proyectocompra/databases/";
	private static String DB_NAME = "compraproyecto.db";
	public SQLiteDatabase myDataBase;
	private static File DATABASE_FILE;
	public static final int DATABASE_VERSION = 1;
	private final Context myContext;

	public BaseDeDatos(Context contexto) {
		super(contexto, DB_NAME, null, DATABASE_VERSION);
		this.myContext = contexto;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// No hacemos nada aqui, porque copiamos la base de datos de assets
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Cuando haya cambios en la estructura deberemos
		updateDataBaseAsync updateDataBase = new updateDataBaseAsync(myContext,db);
		updateDataBase.execute();
	}
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//codigo para cuando se ponga una version anterior de la DB
		Log.i("accion:","Intenta bajar de versión");
	}
	private void updateDataBase(SQLiteDatabase db){
		//aqui pondremos las consultas para actualizar la BD
		//db.execSQL("INSERT INTO departamento VALUES(15, 'Prueba update','1','1','AH')");
		Log.i("Accion", "Actualizando DB");
	}
	public void createDataBase() throws IOException {
		boolean dbExist = checkDataBase();
		if (dbExist) {
			// Si existe, no haemos nada!
			getWritableDatabase();
		}else{
			copyDataBaseAsync copyDataBase = new copyDataBaseAsync(myContext);
			copyDataBase.execute();
		}
	}
	public boolean checkDataBase() {
		boolean existe= false;
		String myPath = DB_PATH + DB_NAME;
		File file = new File(myPath);
		existe=file.exists();
		return  existe;
	}
	public void deleteFileDataBase() {
		boolean existe= false;
		String myPath = DB_PATH + DB_NAME;
		File file = new File(myPath);
		existe=file.exists();
		if(existe){
			file.delete();
		}
	}

	public void openDataBase() throws SQLException {
		// Open the database
		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null,SQLiteDatabase.OPEN_READWRITE);
	}

	public SQLiteDatabase getDataBase() {
		// Abre la base de datos
		try {
			createDataBase();
		} catch (IOException e) {
			throw new Error("Ha sido imposible crear la Base de Datos");
		}

		String myPath = DB_PATH + DB_NAME;
		return SQLiteDatabase.openDatabase(myPath, null,SQLiteDatabase.OPEN_READWRITE);
	}

	@Override
	public synchronized void close() {

		if (myDataBase != null)
			myDataBase.close();

		super.close();
	}

	private void copyDataBase() throws IOException {
		getReadableDatabase();
		OutputStream databaseOutputStream = new FileOutputStream("" + DB_PATH	+ DB_NAME);
		InputStream databaseInputStream;

		byte[] buffer = new byte[1024];
		int length;

		databaseInputStream = myContext.getAssets().open(DB_NAME);
		while ((length = databaseInputStream.read(buffer)) > 0) {
			databaseOutputStream.write(buffer);
		}

		databaseInputStream.close();
		databaseOutputStream.flush();
		databaseOutputStream.close();
		setDatabaseVersion();
		Log.i("Acción", "Copio la base de datos");
	}
	private void setDatabaseVersion() {
		DATABASE_FILE = myContext.getDatabasePath(DB_NAME);
		SQLiteDatabase db = null;
		try {
			db = SQLiteDatabase.openDatabase(DATABASE_FILE.getAbsolutePath(), null,SQLiteDatabase.OPEN_READWRITE);
			db.execSQL("PRAGMA user_version = " + DATABASE_VERSION);
		} catch (SQLiteException e ) {
		} finally {
			if (db != null && db.isOpen()) {
				db.close();
			}
		}
	}
	public class copyDataBaseAsync extends AsyncTask<String, Void, String> {
		private ProgressDialog progressDialog;
		Context contexto;

		public copyDataBaseAsync(Context contexto){
			this.contexto = contexto;
		}
		protected String doInBackground(String... params) {
			try {
				copyDataBase();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			progressDialog.dismiss();

		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(contexto,"","Copiando la Base de datos...",true);
		}

	}
	public class updateDataBaseAsync extends AsyncTask<String, Void, String> {
		private ProgressDialog progressDialog;
		Context contexto;
		SQLiteDatabase db1;

		public updateDataBaseAsync(Context contexto,SQLiteDatabase db){
			this.contexto = contexto;
			db1=db;
		}
		protected String doInBackground(String... params) {
			updateDataBase(db1);
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			progressDialog.dismiss();
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(contexto,"","Actualizando la Base de datos...",true);
		}
	}
}

