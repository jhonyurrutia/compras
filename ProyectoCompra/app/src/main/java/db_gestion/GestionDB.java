package db_gestion;

 /*
 @Autor Moisés Menjivar.
  */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.moy.proyectocompra.Actividad.TablaActividad;
import com.moy.proyectocompra.CatedraDocente.TablaCatedraDocente;
import com.moy.proyectocompra.Detalle.TablaDetalleActividad;
import com.moy.proyectocompra.Docente.TablaDocente;
import com.moy.proyectocompra.Principal.SpinnerObject;
import com.moy.proyectocompra.Principal.SpinnerObjectString;

import java.util.ArrayList;
import java.util.List;

public class GestionDB {
	
	
	//variable Login
	public int codigo_perfil;
	public String nombre ="";
	public String nombre_usuario ="";
	public String password_db ="";



	//variables para mostrar DOCENTE.
	public String nombres_docente   = "";
	public String apellidos_docente = "";
	public int id_sexo	            = 0;
	public int id_estado	        = 0;


	//VARIABLES PARA MOSTRAR CÁTEDRA DOCENE.
	public  String nombre_docentecompleto  = "";
	public  int id_facultad				   = 0;
	public  int id_escuela				   = 0;
	public  int id_catedra                 = 0;

	//VARIABLES PARA MOSTRAR ACTIVIDAD.
	public  String nombre_actividad    = "";
	public  String lugar_actividad     = "";
	public  String objetivo_actividad  = "";
	public  String fecha_inicio        = "";
	public  String fecha_fin           = "";




 //----------------------------------------------------------------------------------------------
	public SQLiteDatabase conexOpen;// contiene la conexion a la base de datos
	private  Context ccontexto;


	public void conectarDB(Context contexto) {
		desConectarDB();
		BaseDeDatos objBaseDeDatos = new BaseDeDatos(ccontexto);
		objBaseDeDatos.openDataBase();
		this.conexOpen = objBaseDeDatos.myDataBase;// guardo la conexion a la base de datos

	}
	public void desConectarDB() {
		if (this.conexOpen != null){
			this.conexOpen.close();
		}
	}

  // Inician todas las funciones a consultar, inserta, eliminar, actualiza a la base...


    /*
		Metodo que consulta el usuario y CONTRASEÑA
		@param user, usuario digitado en el editext del login
		@param password, password digitada en el editext del login
		@param contexto.
		@return  respuesta_db,  si el usuario no existe, si la password es incorrecta
     */
	public int consultarUser(String user, String password,Context contexto) {
		int respuesta_db = 0;
		conectarDB(contexto);//concecto con la base de datos
		Cursor c = conexOpen.rawQuery("SELECT codigo_usuario, clave_usuario, codigo_perfil,nombre_usuario FROM usuario where codigo_usuario='" + user + "'", null);
		if (c.moveToFirst()) {						//	Recorremos el cursor hasta que no haya mas registros.
			do {
					nombre         = c.getString(0);
					password_db    = c.getString(1);
					codigo_perfil  = c.getInt(2);
					nombre_usuario = c.getString(3);
			} while (c.moveToNext());
		}
		if ((nombre.equals(""))) {
			respuesta_db = 0;
		} else {
			if (password_db.equals(password)) {
				respuesta_db = 1;
			} else {
				respuesta_db = 2;
			}
		}
		desConectarDB();
		return respuesta_db;
	}

	//LLENAR COMBO SEXO PROVEEDOR --> "SPINNER"
	public List<SpinnerObjectString> getSexo(Context contexto) {

		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("select  id_sexo, sexo from sexo", null);

		names.add(new SpinnerObjectString(0, "--Seleccione Sexo--"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getInt(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}

	//LLENAR COMBO ESTADO  --> "SPINNER"
	public List<SpinnerObjectString> getEstado(Context contexto) {

		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("select id_estado, estado from estado", null);

		names.add(new SpinnerObjectString(0, "-Seleccione Estado-"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getInt(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}

  /* consulto la cantidad de registros de la tabla DOCENTE, YA SE POR BUSQUEDA DE PARA METRO O NO.
  @param todos los parametros de la tabla docente.
  @return  cantidad_registro
   */

	public int  consultarRegistros(Context contexto, String nombres_docente, String apellidos_docente, int id_sexo, int id_estado, int accion) {
		conectarDB(contexto);
		int cantidad_registro = 0;
		String query = "SELECT COUNT(*) cantidad FROM DOCENTE t01";


		if (accion == 3) {

			query += " WHERE ";

			// NOMBRES DOCENTE
			if (nombres_docente.equals("")) {
				// no pasa nada.
			} else {
				query += "	t01.nombres_docente  LIKE '%" + nombres_docente + "%'		AND";
			}
			// APELLIDOS DOCENTE
			if (apellidos_docente.equals("")) {
				// no pasa nada.
			} else {
				query += "	t01.apellidos_docente  LIKE '%" + apellidos_docente + "%'		AND";
			}

			// ID SEXO DOCENTE
			if (id_sexo == 0) {
				// no pasa nada.
			} else {
				query += "	t01.id_sexo =" + "'" + id_sexo + "' AND";
			}

			// ID ESTADO DOCENTE
			if (id_estado == 0) {
				// no pasa nada.
			} else {
				query += "	t01.id_estado =" + "'" + id_estado + "' AND";
			}

			query = query.substring(0, query.length() - 4);
		}
		//Log.i("mostrar query","query"+query);

			Cursor cursor = conexOpen.rawQuery(query, null);
			if (cursor.moveToFirst()) {
				do {
					cantidad_registro = cursor.getInt(0);
				} while (cursor.moveToNext());
			}
			desConectarDB();
		//Log.i("mostrar query ", "query " + query + "  " + cantidad_registro);
			return cantidad_registro;

	}


	/*
	Metodo que se encarga de generar la tabla para mostrar la informacion del docente
	@param contexto.
	@retun  mFilas, contiene los campos segun la query ejecutada.
	 */
   public ArrayList<TablaDocente> generarTablaDocente(Context contexto, String nombres_docente, String apellidos_docente, int id_sexo, int id_estado, int accion){

		conectarDB(contexto);

	   String query = "SELECT  t01.id_docente,\n" +
			   "             (t01.nombres_docente  ||'  '||  t01.apellidos_docente) nombrecompleto, \n" +
			   "             t02.sexo, t03.estado   FROM  docente t01\n" +
			   "inner join sexo t02 on (t02.id_sexo=t01.id_sexo)\n" +
			   "inner join estado t03 on (t03.id_estado=t01.id_estado)";
		//Creamos una lista actividades generales
		ArrayList<TablaDocente> mFilas = new ArrayList<TablaDocente>();
		//Abrimos la conexion a la base de datos

		if(accion==3) {

			query += " WHERE ";

			// NOMBRES DOCENTE
			if (nombres_docente.equals("")){
               // no pasa nada.
			}else{
				query += "	t01.nombres_docente  LIKE '%" + nombres_docente + "%'		AND";
			}
            // APELLIDOS DOCENTE
			if (apellidos_docente.equals("")){
				// no pasa nada.
			}else{
				query += "	t01.apellidos_docente  LIKE '%" + apellidos_docente + "%'		AND";
			}

			// ID SEXO DOCENTE
			if (id_sexo==0){
				// no pasa nada.
			}else{
				query+="	t01.id_sexo ="+"'"+id_sexo+"' AND";
			}

			// ID ESTADO DOCENTE
			if (id_estado==0){
				// no pasa nada.
			}else{
				query+="	t01.id_estado ="+"'"+id_estado+"' AND";
			}

			query=query.substring(0, query.length()-4);
		}



	   Cursor cursor = conexOpen.rawQuery(query, null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			TablaDocente row = new TablaDocente();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 			= cursor.getString(0);
					row.nombre 		= cursor.getString(1);
					row.sexo 		= cursor.getString(2);
					row.estado	    = cursor.getString(3);
				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new TablaDocente();

					//colocamos los datos en el nuevo objeto
					row.id 			= cursor.getString(0);
					row.nombre 		= cursor.getString(1);
					row.sexo 		= cursor.getString(2);
					row.estado	    = cursor.getString(3);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}




				// ***  MANTENIMIENTO PARA DOCENTE..**


   // Insertat docente.
	public void insertatDocente(Context contexto, String s_nombres_docente, String s_apellidos_docente,int id_sexo, int id_estado){
		conectarDB(contexto);

		ContentValues newvalues= new ContentValues();
		newvalues.put("nombres_docente",s_nombres_docente);
		newvalues.put("apellidos_docente",s_apellidos_docente);
		newvalues.put("id_sexo",id_sexo);
		newvalues.put("id_estado",id_estado);

		conexOpen.insert("docente", null, newvalues);
    }

	//ELIMINAR  DOCENTE
	public void eliminarDocente(Context contexto, String id_docente){

		conectarDB(contexto);
		conexOpen.delete("docente", "id_docente=" + id_docente, null);

		desConectarDB();
	}

	// CARGAR VARIBLES DEL DOCENTE  ---> PARA MOSTRAR  PARA EDITAR!
	public void cargarVariableDocenteEditar(Context contexto, String id_docente){
		conectarDB(contexto);
		String query="SELECT nombres_docente, apellidos_docente, id_sexo, id_estado FROM docente where id_docente="+id_docente;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {
				this.nombres_docente   = cursor.getString(0);
				this.apellidos_docente = cursor.getString(1);
				this.id_sexo	       = cursor.getInt(2);
				this.id_estado	       = cursor.getInt(3);
             } while (cursor.moveToNext());
		}
		desConectarDB();
	}

	//ACTUALIZAR DOCENTE
	public void actualizarDocente(Context contexto,  String nombre_docente, String apellidos_docente, int id_sexo, int id_estado, int id_docente  ){
		conectarDB(contexto);

		ContentValues newValues = new ContentValues();
		newValues.put("nombres_docente", nombre_docente);
		newValues.put("apellidos_docente", apellidos_docente);
		newValues.put("id_sexo", id_sexo);
		newValues.put("id_estado", id_estado);
		conexOpen.update("docente", newValues, "id_docente="+id_docente, null);

		desConectarDB();
	}


	//************** MANTENIMIENTO CÁTEDRA DOCENTE ************* //////////////


	// LLENAR LISTA DOCENTE  ya sea con parametros de busqueda lleno o vacios.
	public ArrayList<SpinnerObject> listaDocente(Context contexto, String filtro) {

		conectarDB(contexto);
		String cadena ="where id_estado=1  and   (nombres_docente   ||' '|| apellidos_docente  )";

		String cadena_filtro="";

		if(filtro.equals(""))
		{
			cadena_filtro="where id_estado=1 ";
		}
		else
		{
			cadena_filtro=cadena+filtro;
		}

		ArrayList<SpinnerObject> names = new ArrayList<SpinnerObject>();
		Cursor cursor = conexOpen.rawQuery("SELECT id_docente,  ( nombres_docente   ||' '|| apellidos_docente   ) AS nombre   FROM  docente    "+cadena_filtro+ "order by nombres_docente asc", null);
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObject(cursor.getInt(0),  cursor.getString(1) ));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}





	//LLENAR  SPINNER  FACULTAD
	public List<SpinnerObjectString> getFacultad(Context contexto) {

		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("SELECT id_facultad, nombre_facultad FROM facultad", null);

		names.add(new SpinnerObjectString(0, "--Seleccione Facultad--"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getInt(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}

	//LLENAR  SPINNER  ESCUELA
	public List<SpinnerObjectString> getEscuela(Context contexto, int id_facultad) {

		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("SELECT id_escuela, nombre_escuela FROM escuela where id_facultad="+id_facultad, null);

		names.add(new SpinnerObjectString(0, "--Seleccione Escuela--"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getInt(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}

	//LLENAR  SPINNER  CÁTEDRA
	public List<SpinnerObjectString> getCatedra(Context contexto, int id_escuela) {

		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("SELECT id_catedra, nombre_catedra FROM catedra where id_escuela="+id_escuela, null);

		names.add(new SpinnerObjectString(0, "--Seleccione Cátedra--"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getInt(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}





	/*
	Metodo que se encarga de generar la tabla para mostrar la informacion de Cátedra docente
	@param contexto. y todos1 los parametros enviados
	@retun  mFilas, contiene los campos segun la query ejecutada.
	 */
	public ArrayList<TablaCatedraDocente> generarTablaCatedraDocente(Context contexto, String filtro, int id_facultad, int id_escuela, int id_catedra, int id_docente,  int accion){

		conectarDB(contexto);

		String query = "SELECT  t01.id_catedra_docente,  ( t03.nombres_docente   ||' '|| t03.apellidos_docente )  nombre,  t05.nombre_facultad, t04.nombre_escuela, \n" +
				"        t02.nombre_catedra         FROM catedra_docente t01 \n" +
				"inner join catedra t02 on (t01.id_catedra=t02.id_catedra)\n" +
				"inner join docente t03 on (t01.id_docente=t03.id_docente)\n" +
				"inner join escuela t04 on (t02.id_escuela=t04.id_escuela)\n" +
				"inner join facultad t05 on (t04.id_facultad=t05.id_facultad)";
		//Creamos una lista actividades generales
		ArrayList<TablaCatedraDocente> mFilas = new ArrayList<TablaCatedraDocente>();
		//Abrimos la conexion a la base de datos

		if (accion == 3) {

			query += " WHERE ";

			// NOMBRES DOCENTE
			if(filtro.equals(""))
			{
				// ID  DOCENTE
				if (id_docente == 0) {
					// no pasa nada.
				} else {
					query += "	t01.id_docente =" + "'" + id_docente + "' AND";
				}
			}
			else
			{
				query+="    (t03.nombres_docente   ||' '|| t03.apellidos_docente)  LIKE '%"+filtro+"%'  AND";
			}


			// ID facultad
			if (id_facultad == 0) {
				// no pasa nada.
			} else {
				query += "	t05.id_facultad =" + "'" + id_facultad + "' AND";
			}


			if (id_escuela == 0) {

			} else {
				query += "	t04.id_escuela =" + "'" + id_escuela + "' AND";
			}

			if (id_catedra == 0) {

			} else {
				query += "	t02.id_catedra =" + "'" + id_catedra + "' AND";
			}

			query = query.substring(0, query.length() - 4);
		}

        //  Log.i("qrery_catedradocente", ""+query);

		Cursor cursor = conexOpen.rawQuery(query, null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			TablaCatedraDocente row = new TablaCatedraDocente();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 			 = cursor.getString(0);
					row.nombre 		 = cursor.getString(1);
					row.id_facultad  = cursor.getString(2);
					row.id_escuela	 = cursor.getString(3);
					row.id_catedra	 = cursor.getString(4);
				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new TablaCatedraDocente();

					//colocamos los datos en el nuevo objeto
					row.id 			 = cursor.getString(0);
					row.nombre 		 = cursor.getString(1);
					row.id_facultad  = cursor.getString(2);
					row.id_escuela	 = cursor.getString(3);
					row.id_catedra	 = cursor.getString(4);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}



	// Insertar CÁTEDRA DOCENTE .
	public void insertatCatedraDocente(Context contexto,int id_catedra, int id_docente){
		conectarDB(contexto);

		ContentValues newvalues= new ContentValues();
		newvalues.put("id_catedra",id_catedra);
		newvalues.put("id_docente",id_docente);


		conexOpen.insert("catedra_docente", null, newvalues);
	}


	 /* consulto la cantidad de registros de la tabla DOCENTE, YA SE POR BUSQUEDA DE PARA METRO O NO.
  @param todos los parametros de la tabla docente.
  @return  cantidad_registro
   */

	public int  consultarRegistrosCatedraDocente(Context contexto, String filtro, int id_facultad, int id_escuela, int id_catedra,int id_docente, int accion) {
		conectarDB(contexto);
		int cantidad_registro = 0;
		String query = "SELECT  count(*) cantidad FROM catedra_docente t01 \n" +
				"inner join catedra t02 on (t01.id_catedra=t02.id_catedra)\n" +
				"inner join docente t03 on (t01.id_docente=t03.id_docente)\n" +
				"inner join escuela t04 on (t02.id_escuela=t04.id_escuela)\n" +
				"inner join facultad t05 on (t04.id_facultad=t05.id_facultad)";


		if (accion == 3) {

			query += " WHERE ";

			// NOMBRES DOCENTE
              if(filtro.equals(""))
			{
				// ID  DOCENTE
				if (id_docente == 0) {
					// no pasa nada.
				} else {
					query += "	t01.id_docente =" + "'" + id_docente + "' AND";
				}
			}
			else
			{
				query+="    (t03.nombres_docente   ||' '|| t03.apellidos_docente)  LIKE '%"+filtro+"%'  AND";
			}


			// ID facultad
			if (id_facultad == 0) {
				// no pasa nada.
			} else {
				query += "	t05.id_facultad =" + "'" + id_facultad + "' AND";
			}


			if (id_escuela == 0) {

			} else {
				query += "	t04.id_escuela =" + "'" + id_escuela + "' AND";
			}

			if (id_catedra == 0) {

			} else {
				query += "	t02.id_catedra =" + "'" + id_catedra + "' AND";
			}

			query = query.substring(0, query.length() - 4);
		}
		//Log.i("mostrar query","query"+query);

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {
				cantidad_registro = cursor.getInt(0);
			} while (cursor.moveToNext());
		}
		desConectarDB();
		//Log.i("mostrar query ", "query " + query + "  " + cantidad_registro);
		return cantidad_registro;

	}


	//ELIMINAR  CÁTEDRA  DOCENTE
	public void eliminarCatedraDocente(Context contexto, String id_catedra_docente){

		conectarDB(contexto);
		conexOpen.delete("catedra_docente", "id_catedra_docente=" + id_catedra_docente, null);

		desConectarDB();
	}


	// CARGAR VARIBLES DE LA CATEDRA DOCENTE  ---> PARA MOSTRAR,  PARA EDITAR!
	public void cargarVariableCatedraDocenteEditar(Context contexto, String id_catedra_docente){
		conectarDB(contexto);
		String query="SELECT  ( t03.nombres_docente   ||' '|| t03.apellidos_docente )  nombre,   t05.id_facultad, \n" +
				"              t04.id_escuela, t02.id_catedra\n" +
				"                  FROM catedra_docente t01 \n" +
				"                 inner join catedra t02 on (t01.id_catedra=t02.id_catedra)\n" +
				"                 inner join docente t03 on (t01.id_docente=t03.id_docente)\n" +
				"                 inner join escuela t04 on (t02.id_escuela=t04.id_escuela)\n" +
				"                 inner join facultad t05 on (t04.id_facultad=t05.id_facultad) where t01.id_catedra_docente="+id_catedra_docente;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {
				this.nombre_docentecompleto = cursor.getString(0);
				this.id_facultad            = cursor.getInt(1);
				this.id_escuela	            = cursor.getInt(2);
				this.id_catedra	            = cursor.getInt(3);
			} while (cursor.moveToNext());
		}
		desConectarDB();
	}


	//ACTUALIZAR CÁTEDRA  DOCENTE
	public void actualizarCatedraDocente(Context contexto,   int id_catedra, int id_docente, int i_id_catedra_docente  ){
		conectarDB(contexto);

		ContentValues newValues = new ContentValues();
		newValues.put("id_catedra", id_catedra);
		newValues.put("id_docente", id_docente);

		conexOpen.update("catedra_docente", newValues, "id_catedra_docente="+i_id_catedra_docente, null);

		desConectarDB();
	}

	// obtener id del docente por medio del nombre digitado en el editex
	public int  obtenerIdDocentePorNombre(Context contexto, String nombre_docente){
		conectarDB(contexto);
		int id_docente=0;
		String query="SELECT  id_docente from docente where (nombres_docente   ||' '|| apellidos_docente)='"+nombre_docente+"'";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {
				id_docente = cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return  id_docente;
	}

             //********************* MANTENIMIENTO ACTIVIDAD **************


	// Insertar Actividad
	public void insertatActividad(Context contexto,int id_docente, String actividad, String lugar, String objetivo, String fecha_inicio, String fecha_fin ){
		conectarDB(contexto);

		ContentValues newvalues= new ContentValues();

		newvalues.put("id_docente",id_docente);
		newvalues.put("nombre_actividad",actividad);
		newvalues.put("lugar_actividad",lugar);
		newvalues.put("objetivo_actividad",objetivo);
		newvalues.put("fecha_inicio",fecha_inicio);
		newvalues.put("fecha_fin", fecha_fin);

       conexOpen.insert("actividad", null, newvalues);
	}



	 /* consulto la cantidad de registros de la tabla Actividad, YA SE POR BUSQUEDA DE PARAMETRO O NO.
  @param todos los parametros de la tabla docente.
  @return  cantidad_registro
   */

	public int  consultarRegistrosActividad(Context contexto, String filtro, String actividad, String lugar, String objetivo, String  fecha_inicio, String  fecha_fin,  int id_docente, int accion) {
		conectarDB(contexto);
		int cantidad_registro = 0;
		String query = "SELECT  COUNT(*) cantidad  FROM actividad t01 inner join docente t02 on (t01.id_docente=t02.id_docente)";


		if (accion == 3) {

			query += " WHERE ";

			// NOMBRES DOCENTE
			if(filtro.equals(""))
			{
				// ID  DOCENTE
				if (id_docente == 0) {
					// no pasa nada.
				} else {
					query += "	t01.id_docente =" + "'" + id_docente + "' AND";
				}
			}
			else
			{
				query+="    (t02.nombres_docente   ||' '|| t02.apellidos_docente)  LIKE '%"+filtro+"%'  AND";
			}


			// actividad
			if (actividad.equals("")) {
				// no pasa nada.
			} else {
				query+="    (t01.nombre_actividad  LIKE '%"+actividad+"%'  AND";
			}

           // lugar
			if (lugar.equals("")) {

			} else {
				query+="    (t01. lugar_Actividad  LIKE '%"+lugar+"%'  AND";
			}

			//objetivo
			if (objetivo.equals("")) {

			} else {
				query+="    (t01.objetivo_actividad  LIKE '%"+objetivo+"%'  AND";
			}

			//fecha inicio
			if(fecha_inicio.equals("")){

			}else{
				query += "	t01.fecha_inicio =" + "'" + fecha_inicio + "' AND";
			}

			//fecha fin
			if(fecha_fin.equals("")){

			}else{
				query += "	t01. fecha_fin =" + "'" + fecha_fin + "' AND";
			}

			query = query.substring(0, query.length() - 4);
		}
		//Log.i("mostrar query","query"+query);

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {
				cantidad_registro = cursor.getInt(0);
			} while (cursor.moveToNext());
		}
		desConectarDB();
		Log.i("mostrar query ", "query " + query + "  " + cantidad_registro);
		return cantidad_registro;

	}



	/*
	Metodo que se encarga de generar la tabla para mostrar la informacion de Cátedra docente
	@param contexto. y todos1 los parametros enviados
	@retun  mFilas, contiene los campos segun la query ejecutada.
	 */
	public ArrayList<TablaActividad> generarTablaActividad(Context contexto, String filtro, String actividad, String lugar, String objetivo, String  fecha_inicio, String  fecha_fin,  int id_docente, int accion){

		conectarDB(contexto);

		String query = "SELECT  t01.id_actividad, (t02.nombres_docente   ||' '|| t02.apellidos_docente), \n" +
				"t01.nombre_actividad ,t01.lugar_actividad, t01.objetivo_actividad, t01.fecha_inicio, t01.fecha_fin \n" +
				" FROM actividad t01 inner join docente t02 on (t01.id_docente=t02.id_docente)";
		//Creamos una lista actividades generales
		ArrayList<TablaActividad> mFilas = new ArrayList<TablaActividad>();
		//Abrimos la conexion a la base de datos

		if (accion == 3) {

			query += " WHERE ";

			// NOMBRES DOCENTE
			if(filtro.equals(""))
			{
				// ID  DOCENTE
				if (id_docente == 0) {
					// no pasa nada.
				} else {
					query += "	t01.id_docente =" + "'" + id_docente + "' AND";
				}
			}
			else
			{
				query+="    (t02.nombres_docente   ||' '|| t02.apellidos_docente)  LIKE '%"+filtro+"%'  AND";
			}


			// actividad
			if (actividad.equals("")) {
				// no pasa nada.
			} else {
				query+="    (t01.nombre_actividad  LIKE '%"+actividad+"%'  AND";
			}

			// lugar
			if (lugar.equals("")) {

			} else {
				query+="    (t01. lugar_Actividad  LIKE '%"+lugar+"%'  AND";
			}

			//objetivo
			if (objetivo.equals("")) {

			} else {
				query+="    (t01.objetivo_actividad  LIKE '%"+objetivo+"%'  AND";
			}

			//fecha inicio
			if(fecha_inicio.equals("")){

			}else{
				query += "	t01.fecha_inicio =" + "'" + fecha_inicio + "' AND";
			}

			//fecha fin
			if(fecha_fin.equals("")){

			}else{
				query += "	t01. fecha_fin =" + "'" + fecha_fin + "' AND";
			}

			query = query.substring(0, query.length() - 4);
		}

		//  Log.i("qrery_catedradocente", ""+query);

		Cursor cursor = conexOpen.rawQuery(query, null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			TablaActividad row = new TablaActividad();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 			 = cursor.getString(0);
					row.nombre 		 = cursor.getString(1);
					row.actividad    = cursor.getString(2);
					row.lugar	     = cursor.getString(3);
					row.objetivo	 = cursor.getString(4);
					row.fecha_inicio = cursor.getString(5);
					row.fecha_fin	 = cursor.getString(6);
				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new TablaActividad();

					//colocamos los datos en el nuevo objeto
					row.id 			 = cursor.getString(0);
					row.nombre 		 = cursor.getString(1);
					row.actividad    = cursor.getString(2);
					row.lugar	     = cursor.getString(3);
					row.objetivo	 = cursor.getString(4);
					row.fecha_inicio = cursor.getString(5);
					row.fecha_fin	 = cursor.getString(6);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}


	//ELIMINAR  ACTIVIDAD
	public void eliminarActividad(Context contexto, String id_actividad){

		conectarDB(contexto);
		conexOpen.delete("actividad", "id_actividad=" + id_actividad, null);

		desConectarDB();
	}


	// CARGAR VARIBLES DE LA CATEDRA DOCENTE  ---> PARA MOSTRAR,  PARA EDITAR!
	public void cargarVariableActividadEditar(Context contexto, String id_actividad){
		conectarDB(contexto);
		String query="SELECT (t02.nombres_docente   ||' '|| t02.apellidos_docente ) nombre , t01.nombre_actividad,\n" +
				"             t01.lugar_actividad, t01.objetivo_actividad,\n" +
				"             t01.fecha_inicio, t01.fecha_fin   FROM actividad t01 \n" +
				" inner join docente t02 on (t01.id_docente=t02.id_docente) where id_actividad="+id_actividad;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {
				this.nombre_docentecompleto = cursor.getString(0);
				this.nombre_actividad       = cursor.getString(1);
				this.lugar_actividad	    = cursor.getString(2);
				this.objetivo_actividad	    = cursor.getString(3);
				this.fecha_inicio	        = cursor.getString(4);
				this.fecha_fin	            = cursor.getString(5);


			} while (cursor.moveToNext());
		}
		desConectarDB();
	}


	//ACTUALIZAR ACTIVIDAD
	public void actualizarActividad(Context contexto, int id_docente,  String actividad, String lugar, String objetivo, String  fecha_inicio, String  fecha_fin,   int i_id_actividad  ){
		conectarDB(contexto);

		ContentValues newValues = new ContentValues();
		newValues.put("id_docente", id_docente);
		newValues.put("nombre_actividad", actividad);
		newValues.put("lugar_actividad", lugar);
		newValues.put("objetivo_actividad", objetivo);
		newValues.put("fecha_inicio", fecha_inicio);
		newValues.put("fecha_fin", fecha_fin);

		conexOpen.update("actividad", newValues, "id_actividad="+i_id_actividad, null);

		desConectarDB();
	}



	//************** MANTENIMIENTO DETALLE ACTVIDAD  ************* //////////////


	// LLENAR LISTA ACTIVIDAD  ya sea con parametros de busqueda lleno o vacios.
	public ArrayList<SpinnerObject> listaActividad(Context contexto, String filtro) {

		conectarDB(contexto);
		String cadena_filtro="";

		if(filtro.equals(""))
		{
			cadena_filtro=cadena_filtro;
		}
		else
		{
			cadena_filtro="where nombre_actividad "+filtro;
		}
          //Log.i("cadena_filtro",""+cadena_filtro);
		ArrayList<SpinnerObject> names = new ArrayList<SpinnerObject>();
		Cursor cursor = conexOpen.rawQuery("SELECT id_actividad, nombre_actividad FROM actividad    "+cadena_filtro+ "order by  nombre_actividad asc", null);
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObject(cursor.getInt(0),  cursor.getString(1) ));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}

	 /* consulto la cantidad de registros de la tabla Actividad.
  		@param contexto.
  		@return  cantidad_registro
   */

	public int  consultarRegistrosDetalleActividad(Context contexto) {
		conectarDB(contexto);
		int cantidad_registro = 0;
		String query = "SELECT  COUNT(*) FROM temporal_detalle_actividad";

        Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {
				cantidad_registro = cursor.getInt(0);
			} while (cursor.moveToNext());
		}
		desConectarDB();
		//Log.i("mostrar query ", "query " + query + "  " + cantidad_registro);
		return cantidad_registro;

	}


	/*
	Metodo que se encarga de generar la tabla para mostrar la informacion de detalle de la actividad
	@param contexto.
	@retun  mFilas, contiene los campos segun la query ejecutada.
	 */
	public ArrayList<TablaDetalleActividad> generarTablaDetalleActividad(Context contexto){

		conectarDB(contexto);

		String query = "SELECT id_detalle_actividad, cantidad_producto, precio_estimado, caracteristicas, proveedor, \n" +
				"              descripcion, total_estimado FROM temporal_detalle_actividad ";
		//Creamos una lista actividades generales
		ArrayList<TablaDetalleActividad> mFilas = new ArrayList<TablaDetalleActividad>();

		Cursor cursor = conexOpen.rawQuery(query, null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			TablaDetalleActividad row = new TablaDetalleActividad();

			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {




				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 			 	= cursor.getString(0);
					row.cantidad 		= cursor.getString(1);
					row.precio    		= cursor.getString(2);
					row.caracteristicas = cursor.getString(3);
					row.proeveedor	    = cursor.getString(4);
					row.descripcion 	= cursor.getString(5);
					row.total	 		= cursor.getString(6);
				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new TablaDetalleActividad();

					//colocamos los datos en el nuevo objeto
					row.id 			 	= cursor.getString(0);
					row.cantidad 		= cursor.getString(1);
					row.precio    		= cursor.getString(2);
					row.caracteristicas = cursor.getString(3);
					row.proeveedor	    = cursor.getString(4);
					row.descripcion 	= cursor.getString(5);
					row.total	 		= cursor.getString(6);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}


	// Insertar detalle de la  Actividad
	public void insertatDetalleActividad(Context contexto, String cantidad, String precio, String caracteristicas, String proveedor, String descripcion, String total, int id_solicitud ){
		conectarDB(contexto);

		ContentValues newvalues= new ContentValues();

		newvalues.put("cantidad_producto",cantidad);
		newvalues.put("precio_estimado",precio);
		newvalues.put("caracteristicas",caracteristicas);
		newvalues.put("proveedor",proveedor);
		newvalues.put("descripcion",descripcion);
		newvalues.put("total_estimado", total);
		newvalues.put("id_solicitud", id_solicitud);

        conexOpen.insert("temporal_detalle_actividad ", null, newvalues);
	}

	// consultar la fecha actual
	public String consultarfecha(Context contexto){
		conectarDB(contexto);
		String fecha="";
		String query="SELECT date('now')  fecha;";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				 fecha	=cursor.getString(0);


			} while (cursor.moveToNext());
		}
		desConectarDB();
		return fecha;
	}

	// Insertar solicitud
	public void insertatSolicitud(Context contexto, int  id_actividad, String fecha_solicitud ){
		conectarDB(contexto);

		ContentValues newvalues= new ContentValues();

		newvalues.put("id_actividad",id_actividad);
		newvalues.put("fecha_solicitud",fecha_solicitud);
		conexOpen.insert("solicitud ", null, newvalues);
	}

	// consultar el ultimo registro solicitud
	public int consultaUltimaSolicitud(Context contexto){
		conectarDB(contexto);
		int id_solicitud=0;
		String query="SELECT MAX(id_solicitud)id_solicitud FROM solicitud";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				id_solicitud	=cursor.getInt(0);
	} while (cursor.moveToNext());
		}
		desConectarDB();
		return id_solicitud;
	}


	//ELIMINAR  solicitud
	public void eliminarDetalle(Context contexto, int id_solicitud){

		conectarDB(contexto);
		conexOpen.delete("temporal_detalle_actividad", "id_solicitud=" + id_solicitud, null);

		desConectarDB();
	}

	//ELIMINAR detalle solicitud
	public void eliminarSolicitud(Context contexto, int id_solicitud){

		conectarDB(contexto);
		conexOpen.delete("solicitud", "id_solicitud=" + id_solicitud, null);

		desConectarDB();
	}



	// INSERTAR DETALLE APROBADO
	public void insertarTodosLosTetalles(Context contexto   ){

		conectarDB(contexto);
		String query="select    cantidad_producto, precio_estimado, caracteristicas, proveedor,descripcion," +
				"               total_estimado, id_solicitud  from temporal_detalle_actividad  ";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				ContentValues newValues = new ContentValues();


				newValues.put("cantidad_producto",cursor.getString(0));
				newValues.put("precio_estimado"  ,cursor.getString(1));
				newValues.put("caracteristicas"  ,cursor.getString(2));
				newValues.put("proveedor"        ,cursor.getString(3));
				newValues.put("descripcion"      ,cursor.getString(4));
				newValues.put("total_estimado"   , cursor.getString(5));
				newValues.put("id_solicitud"     , cursor.getString(6));



				conexOpen.insert("detalle_actividad", null, newValues);


			} while (cursor.moveToNext());
		}



		desConectarDB();
	}

}//termina la clase