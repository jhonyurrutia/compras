/**
 *Esta clase representa los datos dde una fila
 *de la tabla de actividades del reporte A 
 */
package com.moy.proyectocompra.Actividad;

import java.util.ArrayList;

public class TablaActividad {
	public String nombre;
	public String actividad;
	public String lugar;
	public String objetivo;
	public String fecha_inicio;
	public String fecha_fin;

	public String id;





	// / NOMBRECOMPLETO

	public String getnombre() {
		return nombre;
	}
	public void setnombre(String nombre) {
		nombre = nombre;
	}


		// actividad
	public String getactividad() {
		return actividad;
	}
	public void setactividad(String actividad) {
		actividad = actividad;
	}


	//lugar
	public String getlugar() {
		return lugar;
	}
	public void setlugar(String lugar) {
		lugar = lugar;
	}


	// objetivo
	public String getobjetivo() {
		return objetivo;
	}
	public void setebjetivo(String objetivo) {
		objetivo = objetivo;
	}

	// fecha_inicio
	public String getfecha_inicio() {
		return fecha_inicio;
	}
	public void setfecha_inicio(String fecha_inicio) {
		fecha_inicio = fecha_inicio;
	}

	// fecha_fin
	public String getfecha_fin() {
		return fecha_fin;
	}
	public void setfecha_fin(String fecha_fin) {
		fecha_fin = fecha_fin;
	}

    // ID Actividad
	public String getid() {
		return id;
	}
	public void setid(String id) {
		id = id;
	}
	

	
	

}
