package com.moy.proyectocompra.Docente;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.moy.proyectocompra.Principal.SpinnerObjectString;
import com.moy.proyectocompra.R;

import java.util.ArrayList;
import java.util.List;

import db_gestion.GestionDB;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Docente.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Docente#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Docente extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    public GestionDB objGestionDB;
    Dialog customDialog=null;
    EditText e_nombres_docente, e_apellidos_docente;
    Spinner sp_sexo, sp_estado;
    ImageView guardar, buscar, editar, cargar;
    LinearLayout tablavacia;

    int accion         = 0;
    int i_id_docente   = 0;
    int id_sexo        = 0;
    int id_estado      = 0;
    int n_id_sexo      = 0;
    int n_id_estado    = 0;
    int accion_guardar = 1;
    int accion_editar  = 2;
    int accion_buscar  = 3;


    String nombres_docente    = "";
    String apellidos_docente = "";

    private TableLayout mTableLayoutReporte;
    private ArrayList<TablaDocente> mListaActividades;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Docente() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Docente.
     */
    // TODO: Rename and change types and number of parameters
    public static Docente newInstance(String param1, String param2) {
        Docente fragment = new Docente();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        //Inflamos el gragmennto y creamo el view para poder acceder a los controles del fragmento
        View view = inflater.inflate(R.layout.fragment_docente, container, false);

        objGestionDB = new GestionDB(); // creo objeto gestion.

        // INICIALIZO CONTROLES
        e_nombres_docente   = (EditText)view.findViewById(R.id.Docente_editTextnombres);
        e_apellidos_docente = (EditText)view.findViewById(R.id.Docente_editTextApellidos);
        sp_sexo             = (Spinner)view.findViewById(R.id.Docente_spinnerSexo);
        sp_estado           = (Spinner) view.findViewById(R.id.Docente_spinnerEstado);
        mTableLayoutReporte = (TableLayout)view.findViewById(R.id.tablelayout_docente);
        guardar             = (ImageView)view.findViewById(R.id.Docente_imageView_guardar);
        buscar              = (ImageView)view.findViewById(R.id.Docente_imageView_buscar);
        editar              = (ImageView)view.findViewById(R.id.Docente_imageView_Editar);
        cargar              = (ImageView)view.findViewById(R.id.Docente_imageView_cancelarCargar);
        tablavacia          = (LinearLayout)view.findViewById(R.id.Docente_linerlayout_tablavacia);

        tablavacia.setVisibility(View.INVISIBLE);
        if(accion==0){
            editar.setVisibility(View.INVISIBLE);
           // cargar.setVisibility(View.INVISIBLE);
        }
        //CLICK BNT GUARDAR
        guardar.setOnClickListener(new View.OnClickListener() {
             public void onClick(View v) {
                 accion=accion_guardar;
                 VerificarCaposGuardarDocente(accion);
             }
        });

        //CLICK BNT CARGAR
       cargar.setOnClickListener(new View.OnClickListener() {
              public void onClick(View v) {
                accion=accion_guardar;
                setiarCampos();
             }
        });

        //CLICK BNT EDITAR
        editar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                accion=accion_editar;
                VerificarCaposGuardarDocente(accion);
            }
        });

        //CLICK BNT BUSCAR
        buscar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                clickBuscar();
            }
        });




        cargarspinersexo();
        cargarspinerestado();
        CrearConsultaMostrarTablaDocente();

        // OPTENER ID DEL COMBO SEXO SELECCIONADO
        sp_sexo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                id_sexo = (((SpinnerObjectString) sp_sexo.getSelectedItem()).getId());

            }
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

         // OPTENER ID DEL COMBO SEXO SELECCIONADO
        sp_estado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                id_estado = (((SpinnerObjectString) sp_estado.getSelectedItem()).getId());

            }
             public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });



        return view;
    }

    public void cargarspinersexo(){
        // CARGAR COMBO DE SEXO --> SPINNER

        n_id_sexo=objGestionDB.id_sexo;

        List<SpinnerObjectString> lables = objGestionDB.getSexo(getContext());
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(getContext(), android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_sexo.setAdapter(dataAdapter);

        if(accion==accion_editar){
            int pos=0;
            for(int i=0; i<lables.size(); i++){
                if(lables.get(i).getId()==n_id_sexo){
                    pos=i;
                }
            }
            sp_sexo.setSelection(pos);
        }


    }

    public void cargarspinerestado(){
        // CARGAR COMBO DE ESTADO --> SPINNER

        n_id_estado=objGestionDB.id_estado;

        List<SpinnerObjectString> lables = objGestionDB.getEstado(getContext());
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(getContext(), android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_estado.setAdapter(dataAdapter);

        if(accion==accion_editar){
            int pos=0;
            for(int i=0; i<lables.size(); i++){
                if(lables.get(i).getId()==n_id_estado){
                    pos=i;
                }
            }
            sp_estado.setSelection(pos);
        }

    }




    /*MANDO A LLAMAR LE METODO EN LA GESTIONDB QUE SE ENCARGAR DE RETORNAR UN ARRAY
    */
    public void CrearConsultaMostrarTablaDocente(){
        //verifico si hay registros en la base
        int cantidad_registro= objGestionDB.consultarRegistros(getContext(), nombres_docente, apellidos_docente, id_sexo, id_estado, accion);
       if(cantidad_registro<1){
           tablavacia.setVisibility(View.VISIBLE);
           mTableLayoutReporte.removeAllViews();
           mTableLayoutReporte.removeAllViewsInLayout();

        } else {
           tablavacia.setVisibility(View.INVISIBLE);
           mListaActividades = objGestionDB.generarTablaDocente(getContext(), nombres_docente, apellidos_docente, id_sexo, id_estado, accion);
           crearTabla();
       }
    }

    // CREO LA TABLA Y LA PERSONALIZO PARA MOSTRAR LOS DATOS QUE ME INTERESAN.
    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion

        for (TablaDocente fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(getContext());

            //Creamos el texview donde se mostrara la informacion
            TextView textViewNombre_docente= new TextView(getContext());
            TextView textViewSexo       = new TextView(getContext());
            TextView textViewEstado     = new TextView(getContext());
            TextView id                 = new TextView(getContext());

            //Colocamos la nombre a mostrar en la columna
            textViewNombre_docente.setText((fila.getnombre()));
            textViewSexo.setText((fila.getsexo()));
            textViewEstado.setText(fila.getestado());
            id.setText((fila.getid()));

            //Asignamos el color al text
            colortext(textViewNombre_docente);
            colortext(textViewSexo);
            colortext(textViewEstado);

            //*** COLUMNA NOMBRE
            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreproductoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreproductoTableRowParams.setMargins(1, 1, 1, 1);
            nombreproductoTableRowParams.width = 298;
            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombre_docente, nombreproductoTableRowParams);
            //*** FIN COLUMNA NOMBRE

            //*** COLUMNA CODIGO
            TableRow.LayoutParams codigoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            codigoTableRowParams.setMargins(1, 1, 1, 1);
            codigoTableRowParams.width = 101;
            tableRow.addView(textViewSexo, codigoTableRowParams);
            //***FIN COLUMNA CODIGO.

            //*** columna  ESTADO.
            TableRow.LayoutParams estadoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            estadoTableRowParams.setMargins(1, 1, 1, 1);
            estadoTableRowParams.width = 100;
            tableRow.addView(textViewEstado, estadoTableRowParams);
            //*** fin columna estado.

            //Creamos el linear layout que contendra las actividades de la fecha
            LinearLayout layout = new LinearLayout(getContext());
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            //Agregamos las actividades a la fila de la tabla para que utilicen la segunda columna
            // tableRow.addView(layout, activdadTableRowParams);
            final String s_id 	    = id.getText().toString().trim();

            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                       //tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                       alertaEliminarEditar(s_id);
                 }
            });
            //Agregamos la fila creada a la tabla de la pantalla
            mTableLayoutReporte.addView(tableRow);
        }
    }

    // metodo que se encargar de setiar el  color a los editex y el Size.
    public  void colortext( TextView text){

        text.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

        //Colocamos la orientacion del texto, tamaño, el estilo negritas y la altura del view
        text.setGravity(Gravity.CENTER);
        text.setTextSize(15);
        text.setTypeface(null, Typeface.BOLD);
        text.setHeight(50);
    }

   // VERIFICO LOS CAMPOS PARA PODER GUARDAR O EDITATR EL REGISTRO.
    public  void  VerificarCaposGuardarDocente(int accion){
        //Toast.makeText(getContext(), "clickio el boton guardar", Toast.LENGTH_SHORT).show();

        int respuesta_validar = validarCampos(accion_guardar);
        if(respuesta_validar==0){
             if(accion==accion_guardar){
                 objGestionDB.insertatDocente(getContext(), nombres_docente, apellidos_docente, id_sexo, id_estado);
                 alertaConfirmacion("Docente Ingresado Correctamente..");

             }else{
                // Toast.makeText(getContext(), "clickio "+i_id_docente, Toast.LENGTH_SHORT).show();
                 objGestionDB.actualizarDocente(getContext(), nombres_docente, apellidos_docente, id_sexo, id_estado, i_id_docente);
                 alertaConfirmacion("Docente Editado Correctamente..");
             }
             setiarCampos();
        }else {
            alertaCompletarCamposDocente("Complete los Campos");
        }
    }

   // click del boton buscar
    public void   clickBuscar(){

        int respuesta_validar = validarCampos(accion_buscar);
        if(respuesta_validar==0){
            // VERIFICO SI HAY REGISTROS EN LA BASE SEGUN LOS PARAMENTROS DE BUSQUEDA
            int cantidad_registro= objGestionDB.consultarRegistros(getContext(),nombres_docente, apellidos_docente,id_sexo,id_estado,accion_buscar);
            if(cantidad_registro<1){
                tablavacia.setVisibility(View.VISIBLE);
                mTableLayoutReporte.removeAllViews();
                mTableLayoutReporte.removeAllViewsInLayout();
            }else{
                tablavacia.setVisibility(View.INVISIBLE);
                mListaActividades=objGestionDB.generarTablaDocente(getContext(), nombres_docente, apellidos_docente, id_sexo, id_estado, accion_buscar);
                crearTabla();
            }
        }else {
            alertaCompletarCamposDocente("Ingrese al menos un parametro para realizar la busqueda");
        }
    }

    /*
    VALIDO LOS CAMPOS SEGÚN LA ACCION GUARDAR EDITAR O BUSCAR.
     @PARAM  accion
    @RETURN  respuesta_validar 0 si todolos campos estan completados y 1 si no estan todos completados CUANDO ACCION== GUARDAR O EDITAR
     */
    public  int  validarCampos(int accion){
        int respuesta_validar=0;

        nombres_docente    = e_nombres_docente.getText().toString().trim();
        apellidos_docente = e_apellidos_docente.getText().toString().trim();

       if(accion==accion_guardar){
           if(  (nombres_docente.equals("")) || (apellidos_docente.equals("")) || (id_estado==0)  || (id_sexo==0)  ) {

               respuesta_validar=1;
           }
       }else if (accion==accion_buscar) {
           if(  (nombres_docente.equals("")) && (apellidos_docente.equals("")) && (id_estado==0)  && (id_sexo==0)  ) {

               respuesta_validar=1;
           }
       }
        return  respuesta_validar;
    }

      //ALERT COMPLETAR CAMPOS
    public void alertaCompletarCamposDocente(final String mensaje)
    {

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.error);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloError);
        titulo.setText("Información!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeError);
        contenido.setText(mensaje);

        ((Button) customDialog.findViewById(R.id.btn_aceptarError)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }


    //ALERT CONFIRMACIÓN YA SEA PARA INSERT, DELETE O UPDATE!
    public void alertaConfirmacion(String mensaje)
    {

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("Confirmación!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText(mensaje);

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                setiarCampos();
                CrearConsultaMostrarTablaDocente();
            }
        });
        customDialog.show();
    }

        // ALERTA QUE PREGUNTA QUE HACER SI ELIMINAR O EDITAR EL REGISTRO SELECCIONADO DE LA TABLA.
    public void alertaEliminarEditar( final String id_docente)
    {
        i_id_docente = Integer.parseInt(id_docente);
        // Toast.makeText(getContext(),"seleccionado"+id_docente, Toast.LENGTH_SHORT).show();
        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.modificar_elimar);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloModificarEliminar);
            titulo.setText("Que desea hacer");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeModificarElimar);
        contenido.setText("¿Modificar o Eliminar?");

        ((ImageView) customDialog.findViewById(R.id.img_eliminar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                alertaEliminarAceptarCancelar(id_docente);
            }
        });

        ((ImageView) customDialog.findViewById(R.id.img_editar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                accion = accion_editar;
                setiarCampos(accion, id_docente);

            }
        });

        ((ImageView) customDialog.findViewById(R.id.ima_cerrar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();

            }
        });
        customDialog.show();
    }

  // ALERTA MULTIPLE DE ACEPTAR O CANCELAR LA ACCION DE ELIMINAR.
    public void alertaEliminarAceptarCancelar(final String id_docente){

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.alerta);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloalerta);
        titulo.setText("Eliminar Docente");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajealerta);
        contenido.setText("Una vez que elimines al Docente, no podras deshacer la acción");

        ((Button) customDialog.findViewById(R.id.btn_cancelaraceptarAlerta)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                customDialog.dismiss();
            }
        });

        ((Button) customDialog.findViewById(R.id.btn_aceptarAlerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                objGestionDB.eliminarDocente(getContext(), id_docente);
                customDialog.dismiss();
                alertaConfirmacion("Docente Eliminado correctamente..");
            }
        });

         customDialog.show();
    }

    public void  setiarCampos( ){


            //OCULTO
            editar.setVisibility(View.INVISIBLE);
           // cargar.setVisibility(View.INVISIBLE);
            //Muestro
            guardar.setVisibility(View.VISIBLE);
            buscar.setVisibility(View.VISIBLE);

            e_nombres_docente.setText("");
            e_apellidos_docente.setText("");
            sp_estado.setSelection(0);
            sp_sexo.setSelection(0);

            CrearConsultaMostrarTablaDocente();
    }


    // SETIAR LOS CAMPOS, DEPENDIENDO SI FUE GUARDAR O SI VA EDITAR.
    public void  setiarCampos(int accion, String id_docente){


        if(accion==accion_editar){
                //OCULTO
                guardar.setVisibility(View.INVISIBLE);
            buscar.setVisibility(View.INVISIBLE);
            //MUESTRO
            editar.setVisibility(View.VISIBLE);
                cargar.setVisibility(View.VISIBLE);

            objGestionDB.cargarVariableDocenteEditar(getContext(),id_docente);
            e_nombres_docente.setText(objGestionDB.nombres_docente);
            e_apellidos_docente.setText(objGestionDB.apellidos_docente);
            cargarspinersexo();
            cargarspinerestado();

        }


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



}
