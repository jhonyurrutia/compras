package com.moy.proyectocompra.CatedraDocente;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.moy.proyectocompra.Docente.TablaDocente;
import com.moy.proyectocompra.Principal.SpinnerObject;
import com.moy.proyectocompra.Principal.SpinnerObjectString;
import com.moy.proyectocompra.R;

import java.util.ArrayList;
import java.util.List;

import db_gestion.GestionDB;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CatedraDocente.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CatedraDocente#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CatedraDocente extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public GestionDB objGestionDB;
    Dialog customDialog=null;
    Spinner sp_facultad, sp_escuela, sp_catedra;
    ImageView guardar, buscar, editar, cargar;
    LinearLayout tablavacia;


    int id_docente=0;
    EditText textobuscar;
    String filtro="";
    ListView listaDocente;
    private TableLayout mTableLayoutReporte;
    private ArrayList<TablaCatedraDocente> mListaActividades;



    int i_id_catedra_docente   = 0;
    int id_facultad    = 0;
    int id_escuela     = 0;
    int id_catedra     = 0;
    int n_id_facultad  = 0;
    int n_id_escuela   = 0;
    int n_id_catedra   = 0;
    int accion         = 0;
    int accion_guardar = 1;
    int accion_editar  = 2;
    int accion_buscar  = 3;

    private OnFragmentInteractionListener mListener;

    public CatedraDocente() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CatedraDocente.
     */
    // TODO: Rename and change types and number of parameters
    public static CatedraDocente newInstance(String param1, String param2) {
        CatedraDocente fragment = new CatedraDocente();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        objGestionDB = new GestionDB(); // creo objeto gestion.

        //Inflamos el gragmennto y creamo el view para poder acceder a los controles del fragmento
        View view = inflater.inflate(R.layout.fragment_catedra_docente, container, false);

        inicializarControles(view);
        loadListaDocente();
        cargarSpinnerFacultad();
        CrearConsultaMostrarTablaCatedraDocente();
        clickImagenes();       // llamo los onclik de las imagenes
        obtenerIdLista();      // llamo el metodo donde estan el onclik de la lista docente
        obtnerIdSpennirs();    // llamo el metodo donde estan los onclik de los 3 spinner
        buscarDocenteFiltro(); // llamo el metodo que busca el docente de la lista segun el filtro digitado

        return view;
    }

       /*
       iniicalizo los controles a usar.
        */
    public  void inicializarControles(View view){


        sp_facultad         = (Spinner)view.findViewById(R.id.Catedra_Docente_spinnerFacultad);
        sp_escuela          = (Spinner)view.findViewById(R.id.Catedra_Docente_spinnerEscuela);
        sp_catedra          = (Spinner)view.findViewById(R.id.Catedra_Docente_spinnerCatedra);
        textobuscar         = (EditText)view.findViewById(R.id.Catedra_Docente_editText_docente);
        listaDocente        = (ListView)view.findViewById(R.id.Catedra_docnete_ListView_docente);
        guardar             = (ImageView)view.findViewById(R.id.Catedra_Docente_imageView_guardar);
        buscar              = (ImageView)view.findViewById(R.id.Catedra_Docente_imageView_buscar);
        editar              = (ImageView)view.findViewById(R.id.Catedra_Docente_imageView_Editar);
        cargar              = (ImageView)view.findViewById(R.id.Catedra_Docente_imageView_cancelarCargar);
        mTableLayoutReporte = (TableLayout)view.findViewById(R.id.tablelayout_catedra_docente);
        tablavacia          = (LinearLayout)view.findViewById(R.id.Catedra_Docente_linerlayout_tablavacia);

        listaDocente.setTextFilterEnabled(true);
        tablavacia.setVisibility(View.INVISIBLE);
        editar.setVisibility(View.INVISIBLE);


    }
     /*
      METODO DONDE ESTAN LOS ONCLIK DE LAS IMAGENES GUARDAR, BUSCAR, EDITAR Y CARGAR
      */
    public  void clickImagenes(){

        //CLICK BNT GUARDAR
        guardar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                accion = accion_guardar;
                VerificarCaposGuardarCatedraDocente(accion);
            }
        });


        //CLICK BNT BUSCAR
        buscar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                clickBuscar();
            }
        });

        //CLICK BNT CARGAR
        cargar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                accion = accion_guardar;
                setiarCampos();
            }
        });


        //CLICK BNT EDITAR
        editar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                accion=accion_editar;
                VerificarCaposGuardarCatedraDocente(accion);
            }
        });
    }


    /*
      MANDO A LLAMAR LE METODO EN LA GESTIONDB QUE SE ENCARGAR DE RETORNAR UN ARRAY
    */
    public void CrearConsultaMostrarTablaCatedraDocente(){
        //verifico si hay registros en la base
        int cantidad_registro= objGestionDB.consultarRegistrosCatedraDocente(getContext(), filtro, id_facultad,  id_escuela,  id_catedra, id_docente, accion);
        if(cantidad_registro<1){
            tablavacia.setVisibility(View.VISIBLE);
            mTableLayoutReporte.removeAllViews();
            mTableLayoutReporte.removeAllViewsInLayout();

        } else {
            tablavacia.setVisibility(View.INVISIBLE);
            mListaActividades = objGestionDB.generarTablaCatedraDocente(getContext() ,filtro, id_facultad, id_escuela, id_catedra, id_docente, accion);
            crearTabla();
        }
    }

    // CREO LA TABLA Y LA PERSONALIZO PARA MOSTRAR LOS DATOS QUE ME INTERESAN.
    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion

        for (TablaCatedraDocente fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(getContext());

            //Creamos el texview donde se mostrara la informacion
            TextView textViewNombre_docente = new TextView(getContext());
            TextView textViewFacultad       = new TextView(getContext());
            TextView textViewEscuela        = new TextView(getContext());
            TextView textViewCatedra        = new TextView(getContext());
            TextView id                     = new TextView(getContext());

            //Colocamos la nombre a mostrar en la columna
            textViewNombre_docente.setText((fila.getnombre()));
            textViewFacultad.setText((fila.getFacultad()));
            textViewEscuela.setText(fila.getEscuela());
            textViewCatedra.setText((fila.getCatedra()));
            id.setText((fila.getid()));

            //Asignamos el color al text
            colortext(textViewNombre_docente);
            colortext(textViewFacultad);
            colortext(textViewEscuela);
            colortext(textViewCatedra);

            //*** COLUMNA NOMBRE Docente
            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreproductoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreproductoTableRowParams.setMargins(1, 1, 1, 1);
            nombreproductoTableRowParams.width = 298;
            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombre_docente, nombreproductoTableRowParams);
            //*** FIN COLUMNA NOMBRE

            //*** COLUMNA FACULTAD
            TableRow.LayoutParams FacultadTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            FacultadTableRowParams.setMargins(1, 1, 1, 1);
            FacultadTableRowParams.width = 201;
            tableRow.addView(textViewFacultad, FacultadTableRowParams);
            //***FIN COLUMNA FACULTAD.

            //*** columna  ESCUELA.
            TableRow.LayoutParams escuelaTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            escuelaTableRowParams.setMargins(1, 1, 1, 1);
            escuelaTableRowParams.width = 201;
            tableRow.addView(textViewEscuela, escuelaTableRowParams);
            //*** fin columna ESCUELA.


            //*** columna  CÁTEDRA.
            TableRow.LayoutParams catedraTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            catedraTableRowParams.setMargins(1, 1, 1, 1);
            catedraTableRowParams.width = 200;
            tableRow.addView(textViewCatedra, catedraTableRowParams);
            //*** fin columna CÁTEDRA.

            //Creamos el linear layout que contendra las actividades de la fecha
            LinearLayout layout = new LinearLayout(getContext());
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            //Agregamos las actividades a la fila de la tabla para que utilicen la segunda columna
            // tableRow.addView(layout, activdadTableRowParams);
            final String s_id 	    = id.getText().toString().trim();

            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    //tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                    alertaEliminarEditar(s_id);
                }
            });
            //Agregamos la fila creada a la tabla de la pantalla
            mTableLayoutReporte.addView(tableRow);
        }
    }

    // metodo que se encargar de setiar el  color a los editex y el Size.
    public  void colortext( TextView text){

        text.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

        //Colocamos la orientacion del texto, tamaño, el estilo negritas y la altura del view
        text.setGravity(Gravity.CENTER);
        text.setTextSize(15);
        text.setTypeface(null, Typeface.BOLD);
        text.setHeight(50);
    }




    /*
   VALIDO LOS CAMPOS SEGÚN LA ACCION GUARDAR EDITAR O BUSCAR.
    @PARAM  accion
   @RETURN  respuesta_validar 0 si todolos campos estan completados y 1 si no estan todos completados CUANDO ACCION== GUARDAR O EDITAR
    */
    public  int  validarCampos(int accion){
        int respuesta_validar=0;

        filtro    = textobuscar.getText().toString().trim();


        if(accion==accion_guardar){
             if( (filtro.equals("")) ){

               // if(id_docente==0 || (id_facultad==0) || (id_escuela==0)  || (id_catedra==0) || (id_docente==0)){
                   respuesta_validar=1;
                //}
            }else if((id_docente==0)) {

                id_docente=objGestionDB.obtenerIdDocentePorNombre(getContext(),filtro);
      Log.i("id_docente","id_docente"+id_docente);
                if((id_facultad==0) || (id_escuela==0)  || (id_catedra==0) || (id_docente==0)){
                    respuesta_validar=1;
                }
            }else if((id_facultad==0) || (id_escuela==0)  || (id_catedra==0) ){
                respuesta_validar=1;
            }
        }// presiono buscar
        else if (accion==accion_buscar) {
            if(  (filtro.equals("")) && (id_facultad==0) && (id_escuela==0)  && (id_catedra==0) && (id_docente==0)  ) {

                respuesta_validar=1;
            }
        }
        return  respuesta_validar;
    }



    // VERIFICO LOS CAMPOS PARA PODER GUARDAR O EDITATR EL REGISTRO.
    public  void  VerificarCaposGuardarCatedraDocente(int accion){
        //Toast.makeText(getContext(), "clickio el boton guardar", Toast.LENGTH_SHORT).show();

        int respuesta_validar = validarCampos(accion_guardar);
        if(respuesta_validar==0){
            if(accion==accion_guardar){
                objGestionDB.insertatCatedraDocente(getContext(), id_catedra, id_docente);
                alertaConfirmacion("Cátedra agregada al Docente  Correctamente..");

            }else{
                // Toast.makeText(getContext(), "clickio "+i_id_docente, Toast.LENGTH_SHORT).show();
               objGestionDB.actualizarCatedraDocente(getContext(), id_catedra, id_docente, i_id_catedra_docente );
                alertaConfirmacion("Registro Editado Correctamente..");
            }
            setiarCampos();
        }else {
            alertaCompletarCamposCatedraDocente("Complete los Campos");
        }
    }

    // click del boton buscar
    public void   clickBuscar(){

        int respuesta_validar = validarCampos(accion_buscar);
        if(respuesta_validar==0){
            // VERIFICO SI HAY REGISTROS EN LA BASE SEGUN LOS PARAMENTROS DE BUSQUEDA
            int cantidad_registro= objGestionDB.consultarRegistrosCatedraDocente(getContext(),  filtro, id_facultad,  id_escuela,  id_catedra, id_docente,accion_buscar);
            if(cantidad_registro<1){
                tablavacia.setVisibility(View.VISIBLE);
                mTableLayoutReporte.removeAllViews();
                mTableLayoutReporte.removeAllViewsInLayout();
            }else{
                tablavacia.setVisibility(View.INVISIBLE);
                mListaActividades=objGestionDB.generarTablaCatedraDocente(getContext(), filtro, id_facultad, id_escuela, id_catedra, id_docente, accion_buscar);
                crearTabla();
            }
        }else {
            alertaCompletarCamposCatedraDocente("Ingrese al menos un parametro para realizar la busqueda");
        }
    }

     /*
     BUSCO DE LA LISTA DOCENTE UN DOCENTE SEGUN EL FILTRO DIGITADO.
      */
    public  void buscarDocenteFiltro(){

        textobuscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (textobuscar.length() != 0) {
                    filtro = " LIKE '%" + textobuscar.getText().toString() + "%' ";
                    loadListaDocente();
                }
            }
        });

    }

   /*
   OBTENGO EL ID DEL ITEM SELECCIONADO DE LA LISTA DOCENTE
    */
    public  void obtenerIdLista(){

        listaDocente.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> elemento, View v, int position, long id) {
                id_docente = ((SpinnerObject) elemento.getItemAtPosition(position)).getId();
                String nombre = ((SpinnerObject) elemento.getItemAtPosition(position)).getValue();
                textobuscar.setText(nombre);
            }
        });
    }


    /*
    OBTENGO LOS ID DE LOS ITEM SELECCIONADO DE CADA ESPINNER POR SE PARADO. FACULTAD, ESCUELA Y CATEDRA.
     */
    public  void obtnerIdSpennirs(){

        // OPTENER ID SELECCIONADO DEL SPINNER FACULTAD
        sp_facultad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                id_facultad = (((SpinnerObjectString) sp_facultad.getSelectedItem()).getId());
                cargarSpinnerEscuela(id_facultad);
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        // OPTENER ID SELECCIONADO DEL SPINNER ESCUELA
        sp_escuela.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                id_escuela = (((SpinnerObjectString) sp_escuela.getSelectedItem()).getId());
                cargarSpinnerCatedra(id_escuela);
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


        //Toast.makeText(getContext(), ""+id_escuela, Toast.LENGTH_SHORT).show();

        // OPTENER ID SELECCIONADO DEL SPINNER CÁTEDRA
        sp_catedra.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

                id_catedra = (((SpinnerObjectString) sp_catedra.getSelectedItem()).getId());
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }



     /*
     Cargo el spinner de la facultad
      */

    public void cargarSpinnerFacultad(){

        n_id_facultad=objGestionDB.id_facultad;

        List<SpinnerObjectString> lables = objGestionDB.getFacultad(getContext());
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(getContext(), android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_facultad.setAdapter(dataAdapter);

        if(accion==accion_editar){

          //  Log.i("spinner facultad", "spinner facultad"+n_id_facultad);
            int pos=0;
            for(int i=0; i<lables.size(); i++){
                if(lables.get(i).getId()==n_id_facultad){
                    pos=i;
                }
            }
            sp_facultad.setSelection(pos);
        }
    }


    public void cargarSpinnerEscuela(int id_facultad){


        n_id_escuela=objGestionDB.id_escuela;

        List<SpinnerObjectString> lables = objGestionDB.getEscuela(getContext(), id_facultad);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(getContext(), android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_escuela.setAdapter(dataAdapter);

       if(accion==accion_editar){
            int pos=0;
            for(int i=0; i<lables.size(); i++){
                if(lables.get(i).getId()==n_id_escuela){
                    pos=i;
                }
            }
            sp_escuela.setSelection(pos);
        }
    }

    public void cargarSpinnerCatedra(int id_escuela){


        n_id_catedra=objGestionDB.id_catedra;

        List<SpinnerObjectString> lables = objGestionDB.getCatedra(getContext(), id_escuela);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(getContext(), android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_catedra.setAdapter(dataAdapter);

        if(accion==accion_editar){
            int pos=0;
            for(int i=0; i<lables.size(); i++){
                if(lables.get(i).getId()==n_id_catedra){
                    pos=i;
                }
            }
            sp_catedra.setSelection(pos);
        }
    }

    /*
    Cargo la lista docente
     */
    private void loadListaDocente()

    {
        List<SpinnerObject> lista =  objGestionDB.listaDocente(getContext(), filtro);
      //  List<SpinnerObject> lista =  objGestionDB.listaDocenteprueba(getContext());


        ArrayAdapter<SpinnerObject> adap = new ArrayAdapter<SpinnerObject>(getContext(), R.layout.simplerow, lista);
        adap.setDropDownViewResource(R.layout.simplerow);
        listaDocente.setAdapter(adap);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    //ALERT COMPLETAR CAMPOS
    public void alertaCompletarCamposCatedraDocente(final String mensaje)
    {

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.error);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloError);
        titulo.setText("Información!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeError);
        contenido.setText(mensaje);

        ((Button) customDialog.findViewById(R.id.btn_aceptarError)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    //ALERT CONFIRMACIÓN YA SEA PARA INSERT, DELETE O UPDATE!
    public void alertaConfirmacion(String mensaje)
    {

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("Confirmación!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText(mensaje);

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                setiarCampos();

            }
        });
        customDialog.show();
    }

    // ALERTA QUE PREGUNTA QUE HACER SI ELIMINAR O EDITAR EL REGISTRO SELECCIONADO DE LA TABLA.
    public void alertaEliminarEditar( final String id_catedra_docente)
    {
        i_id_catedra_docente = Integer.parseInt(id_catedra_docente);
        // Toast.makeText(getContext(),"seleccionado"+id_docente, Toast.LENGTH_SHORT).show();
        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.modificar_elimar);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloModificarEliminar);
        titulo.setText("Que desea hacer");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeModificarElimar);
        contenido.setText("¿Modificar o Eliminar?");

        ((ImageView) customDialog.findViewById(R.id.img_eliminar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                alertaEliminarAceptarCancelar(id_catedra_docente);
            }
        });

        ((ImageView) customDialog.findViewById(R.id.img_editar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                accion = accion_editar;
                setiarCamposEditar(accion, id_catedra_docente);

            }
        });

        ((ImageView) customDialog.findViewById(R.id.ima_cerrar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();

            }
        });
        customDialog.show();
    }



    // ALERTA MULTIPLE DE ACEPTAR O CANCELAR LA ACCION DE ELIMINAR.
    public void alertaEliminarAceptarCancelar(final String id_catedra_docente){

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.alerta);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloalerta);
        titulo.setText("Eliminar Cátedra Docente");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajealerta);
        contenido.setText("Una vez que elimines el registro, no podras deshacer la acción");

        ((Button) customDialog.findViewById(R.id.btn_cancelaraceptarAlerta)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                customDialog.dismiss();
            }
        });

        ((Button) customDialog.findViewById(R.id.btn_aceptarAlerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                objGestionDB.eliminarCatedraDocente(getContext(), id_catedra_docente);
                customDialog.dismiss();
                alertaConfirmacion("Docente Eliminado correctamente..");
            }
        });

        customDialog.show();
    }


    public void  setiarCampos( ){


        //OCULTO
       editar.setVisibility(View.INVISIBLE);

        //Muestro
        guardar.setVisibility(View.VISIBLE);
        buscar.setVisibility(View.VISIBLE);

        textobuscar.setText("");
        filtro="";
        id_docente=0;
        sp_facultad.setSelection(0);
        sp_escuela.setSelection(0);
        sp_catedra.setSelection(0);
        loadListaDocente();


        CrearConsultaMostrarTablaCatedraDocente();
    }



    // SETIAR LOS CAMPOS, DEPENDIENDO SI FUE GUARDAR O SI VA EDITAR.
    public void  setiarCamposEditar(int accion, String id_catedra_docente){


        if(accion==accion_editar){
            //OCULTO
            guardar.setVisibility(View.INVISIBLE);
            buscar.setVisibility(View.INVISIBLE);
            //MUESTRO
            editar.setVisibility(View.VISIBLE);
            cargar.setVisibility(View.VISIBLE);

            objGestionDB.cargarVariableCatedraDocenteEditar(getContext(), id_catedra_docente);
            textobuscar.setText(objGestionDB.nombre_docentecompleto);

            cargarSpinnerFacultad();


        }


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
