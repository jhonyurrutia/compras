/**
 *Esta clase representa los datos dde una fila
 *de la tabla de actividades del reporte A 
 */
package com.moy.proyectocompra.Detalle;

public class TablaDetalleActividad {
	public String cantidad;
	public String precio;
	public String caracteristicas;
	public String proeveedor;
	public String descripcion;
	public String total;

	public String id;





	// / cantidad

	public String getcantidad() {
		return cantidad;
	}
	public void setcantidad(String cantidad) {
		cantidad = cantidad;
	}


		// precio
	public String getprecio() {
		return precio;
	}
	public void setprecio(String precio) {
		precio = precio;
	}


	//caracteristicas
	public String getcaracteristicas() {
		return caracteristicas;
	}
	public void setcaracteristicas(String caracteristicas) {
		caracteristicas = caracteristicas;
	}


	// proeveedor
	public String getproeveedor() {
		return proeveedor;
	}
	public void setproeveedor(String proeveedor) {
		proeveedor = proeveedor;
	}

	// descripcion
	public String getdescripcion() {
		return descripcion;
	}
	public void setdescripcion(String descripcion) {
		descripcion = descripcion;
	}

	// total
	public String gettotal() {
		return total;
	}
	public void settotal(String total) {
		total = total;
	}

    // id detalle de la  Actividad
	public String getid() {
		return id;
	}
	public void setid(String id) {
		id = id;
	}
	

	
	

}
