/**
 *Esta clase representa los datos dde una fila
 *de la tabla de actividades del reporte A 
 */
package com.moy.proyectocompra.CatedraDocente;

import java.util.ArrayList;

public class TablaCatedraDocente {
	public String nombre;
	public String id_facultad;
	public String id_escuela;
	public String id_catedra;
	public String id;
	public ArrayList<String> Actividades = new ArrayList<String>();




	// / NOMBRECOMPLETO

	public String getnombre() {
		return nombre;
	}
	public void setnombre(String nombre) {
		nombre = nombre;
	}


		// FACULTAD
	public String getFacultad() {
		return id_facultad;
	}
	public void setFacultad(String id_facultad) {
		id_facultad = id_facultad;
	}


	//ESCUELA
	public String getEscuela() {
		return id_escuela;
	}
	public void setEscuela(String id_escuela) {
		id_escuela = id_escuela;
	}


	// CÁTEDRA
	public String getCatedra() {
		return id_catedra;
	}
	public void seteCatedra(String id_catedra) {
		id_catedra = id_catedra;
	}

    // ID CÁTEDRA DOCENTE
	public String getid() {
		return id;
	}
	public void setid(String id) {
		id = id;
	}
	

	/*public ArrayList<String> getActividades() {
		return Actividades;
	}
	public void setActividades(ArrayList<String> actividades) {
		Actividades = actividades;
	}*/
	
	

}
