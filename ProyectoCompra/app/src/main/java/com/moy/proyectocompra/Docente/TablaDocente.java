/**
 *Esta clase representa los datos dde una fila
 *de la tabla de actividades del reporte A 
 */
package com.moy.proyectocompra.Docente;

import java.util.ArrayList;

public class TablaDocente {
	public String nombre;
	public String sexo;
	public String estado;
	public String id;
	public ArrayList<String> Actividades = new ArrayList<String>();




	// / NOMBRECOMPLETO

	public String getnombre() {
		return nombre;
	}
	public void setnombre(String nombre) {
		nombre = nombre;
	}


		// SEXO
	public String getsexo() {
		return sexo;
	}
	public void setsexo(String sexo) {
		sexo = sexo;
	}


	// ESTADO
	public String getestado() {
		return estado;
	}
	public void setestado(String estado) {
		estado = estado;
	}








	// ID DOCENTE
	public String getid() {
		return id;
	}
	public void setid(String id) {
		id = id;
	}
	

	/*public ArrayList<String> getActividades() {
		return Actividades;
	}
	public void setActividades(ArrayList<String> actividades) {
		Actividades = actividades;
	}*/
	
	

}
