package com.moy.proyectocompra;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.moy.proyectocompra.Principal.MenuPrincipal;

import java.io.IOException;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class Login extends AppCompatActivity {

    EditText e_codigo_usuario, e_password;
    String s_codigo_usuario,  s_password;
    Dialog customDialog=null;
    public Context contexto = this;
    private static final String PREFRENCES_NAME = "sesionesSharedPreferences";

    public GestionDB objGestionDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        iniciarDb();
        inicializarControles();
    }

    public void iniciarDb (){

        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            this.objGestionDB = new GestionDB();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public  void inicializarControles(){

        e_codigo_usuario = (EditText) findViewById(R.id.Login_editText_usuario);
        e_password       = (EditText) findViewById(R.id.Login_editText_password);
    }


    public  void clickBtnAceptarLogin(View btnaceptar){

        s_codigo_usuario = e_codigo_usuario.getText().toString().trim();
        s_password       = e_password.getText().toString().trim();

        int respuesta=validarCampos();

        if(respuesta==1){
            errorCompletarCamposLogin();

        }
        else {
            respuesta = objGestionDB.consultarUser(s_codigo_usuario, s_password,contexto);// llamo el  metodo que consulta  a la db
            if (respuesta == 0)
            {
               // Toast.makeText(this, "El usuario no existe", Toast.LENGTH_LONG).show();
                e_codigo_usuario.setError("El usuario no existe");
                e_codigo_usuario.requestFocus();
            }
            else if (respuesta == 2)
            {
                //Toast.makeText(this, "La password es incorrecta",Toast.LENGTH_LONG).show();
                e_password.setError("La password es incorrecta");
                e_password.requestFocus();
            }
            else if (respuesta == 1)
            {

                sesiones_SP();
                // Toast.makeText(this, "todo bien",Toast.LENGTH_LONG).show();

                Intent i = new Intent(this, MenuPrincipal.class);
                startActivity(i);
                finish();
            }
            else {
                Toast.makeText(	this,"Ha ocurrido un error no se puedo consultar la informacion",Toast.LENGTH_LONG).show();
            }
        }

    }

    public  int  validarCampos(){
        int respuesta=0;



        if(s_codigo_usuario.equals("")   ){

            e_codigo_usuario.setError("Este Campo es requerido");
            e_codigo_usuario.requestFocus();

            respuesta=1;
        }
        else if(s_password.equals("")){

            e_password.setError("Este Campo es requerido");
            e_password.requestFocus();

            respuesta=1;
        }


        return  respuesta;
    }
    /*
      guardo las variables en shared preferences.
    */
    public void sesiones_SP() {
        SharedPreferences preferencias = getSharedPreferences(PREFRENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();

        //	usuario
        editor.putString("nombreusuario_sp", objGestionDB.nombre_usuario);  // nombre completo del usuario
        editor.putInt("codigo_perfil", objGestionDB.codigo_perfil);			// codigo perfil del usuario
        editor.commit();
    }

    public void errorCompletarCamposLogin()
    {

        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.error);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloError);
        titulo.setText("ERROR!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeError);
        contenido.setText("Complete los Campos");

        ((Button) customDialog.findViewById(R.id.btn_aceptarError)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }


}
