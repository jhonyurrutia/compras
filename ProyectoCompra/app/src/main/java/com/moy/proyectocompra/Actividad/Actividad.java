package com.moy.proyectocompra.Actividad;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.view.View.OnClickListener;
import android.widget.TableRow;
import android.widget.TextView;

import com.moy.proyectocompra.CatedraDocente.TablaCatedraDocente;
import com.moy.proyectocompra.Principal.SpinnerObject;
import com.moy.proyectocompra.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import db_gestion.GestionDB;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Actividad.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Actividad#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Actividad extends Fragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    EditText textobuscar, e_actividad, e_lugar, e_objetivo, e_fecha_inicio, e_fecha_fin;

    ListView listaDocente;
    private TableLayout mTableLayoutReporte;
    private ArrayList<TablaActividad> mListaActividades;
    LinearLayout tablavacia;
    public GestionDB objGestionDB;
    Dialog customDialog=null;
    ImageView guardar, buscar, editar, cargar;
    public ImageButton ibutton_fecha_inicio;
    public ImageButton ibutton_fecha_fin;

    String filtro       = "";
    String fecha_inicio = "";
    String fecha_fin    = "";
    String actividad    = "";
    String lugar        = "";
    String objetivo     = "";

    int i_id_actividad = 0;
    int id_docente     = 0;
    int accion         = 0;
    int accion_guardar = 1;
    int accion_editar  = 2;
    int accion_buscar  = 3;

    boolean b_fecha        = true;
    boolean b_fecha_inicio = false;
    boolean b_fecha_fin    = false;


    private OnFragmentInteractionListener mListener;

    public Actividad() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Actividad.
     */
    // TODO: Rename and change types and number of parameters
    public static Actividad newInstance(String param1, String param2) {
        Actividad fragment = new Actividad();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        objGestionDB = new GestionDB(); // creo objeto gestion.

        //Inflamos el gragmennto y creamo el view para poder acceder a los controles del fragmento
        View view = inflater.inflate(R.layout.fragment_actividad, container, false);
        inicializarControles(view);
        CrearConsultaMostrarTablaActividad();
        loadListaDocente();
        obtenerIdLista();      // llamo el metodo donde estan el onclik de la lista docente
        buscarDocenteFiltro(); // llamo el metodo que busca el docente de la lista segun el filtro digitado
        clickImagenes();       // llamo los onclik de las imagenes
        return view;
    }


    public  void inicializarControles(View view){

        textobuscar          = (EditText)view.findViewById(R.id.Actividad_editText_docente_responsable);
        e_fecha_inicio       = (EditText)view.findViewById(R.id.Actividad_Editext_fechainicio);
        e_fecha_fin          = (EditText)view.findViewById(R.id.Actividad_Editext_fecha_fin);
        e_actividad          = (EditText)view.findViewById(R.id.Actividad_Editext_nombre_actividad);
        e_lugar              = (EditText)view.findViewById(R.id.Actividad_Editext_lugar);
        e_objetivo           = (EditText)view.findViewById(R.id.Actividad_Editext_objetivo);
        listaDocente         = (ListView)view.findViewById(R.id.Actividad_ListView_docente);
        guardar              = (ImageView)view.findViewById(R.id.Actividad_imageView_guardar);
        buscar               = (ImageView)view.findViewById(R.id.Actividad_imageView_buscar);
        editar               = (ImageView)view.findViewById(R.id.Actividad_imageView_Editar);
        cargar               = (ImageView)view.findViewById(R.id.Actividad_imageView_cancelarCargar);
        ibutton_fecha_inicio = (ImageButton)view.findViewById(R.id.Actividad_imageFecha_inici);
        ibutton_fecha_fin    = (ImageButton)view.findViewById(R.id.Actividad_imageFecha_fin);
        mTableLayoutReporte  = (TableLayout)view.findViewById(R.id.tablelayout_Actividad);
        tablavacia           = (LinearLayout)view.findViewById(R.id.Actividad_linerlayout_tablavacia);

        listaDocente.setTextFilterEnabled(true);
        tablavacia.setVisibility(View.INVISIBLE);
        editar.setVisibility(View.INVISIBLE);


        actividad    = e_actividad.getText().toString().trim();
        lugar        = e_lugar.getText().toString().trim();
        objetivo     = e_objetivo.getText().toString().trim();
        fecha_inicio = e_fecha_inicio.getText().toString().trim();
        fecha_fin    = e_fecha_fin.getText().toString().trim();
    }

    /*
    METODO DONDE ESTAN LOS ONCLIK DE LAS IMAGENES GUARDAR, BUSCAR, EDITAR Y CARGAR
    */
    public  void clickImagenes(){

        // CLICK BNT CALENDARIO FECHA INICIO
        ibutton_fecha_inicio.setOnClickListener(new OnClickListener() {
            @SuppressLint("NewApi")
            public void onClick(View v) {
                //@Override
                b_fecha_inicio = true;
                b_fecha_fin = false;
                datePopup();
            }
        });

        // CLICK BNT CALENDARIO FECHA FIN
        ibutton_fecha_fin.setOnClickListener(new OnClickListener() {
            @SuppressLint("NewApi")
            public void onClick(View v) {
                //@Override
                b_fecha_inicio = false;
                b_fecha_fin = true;
                datePopup();
            }
        });


        //CLICK BNT GUARDAR
        guardar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                accion = accion_guardar;
                VerificarCaposGuardarEditar(accion);
            }
        });


        //CLICK BNT BUSCAR
        buscar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                clickBuscar();
            }
        });

        //CLICK BNT CARGAR
        cargar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                accion = accion_guardar;
                setiarCampos();
            }
        });


        //CLICK BNT EDITAR
        editar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                accion = accion_editar;
                VerificarCaposGuardarEditar(accion);
            }
        });

    }


    /*
      MANDO A LLAMAR LE METODO EN LA GESTIONDB QUE SE ENCARGAR DE RETORNAR UN ARRAY para llenar mi tabla lista actividad
    */
    public void CrearConsultaMostrarTablaActividad(){
        //verifico si hay registros en la base
        int cantidad_registro= objGestionDB.consultarRegistrosActividad(getContext(), filtro, actividad,  lugar,  objetivo, fecha_inicio, fecha_fin, id_docente, accion);
        Log.i("cantidad_registro","cantidad_registro"+cantidad_registro);
        if(cantidad_registro<1){
            tablavacia.setVisibility(View.VISIBLE);
            mTableLayoutReporte.removeAllViews();
            mTableLayoutReporte.removeAllViewsInLayout();

        } else {

            tablavacia.setVisibility(View.INVISIBLE);
            mListaActividades = objGestionDB.generarTablaActividad(getContext() ,filtro, actividad,  lugar,  objetivo, fecha_inicio, fecha_fin, id_docente, accion);
            crearTabla();
        }
    }



    // CREO LA TABLA Y LA PERSONALIZO PARA MOSTRAR LOS DATOS QUE ME INTERESAN.
    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion

        for (TablaActividad fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(getContext());

            //Creamos el texview donde se mostrara la informacion
            TextView textViewNombre_docente = new TextView(getContext());
            TextView textViewActividad       = new TextView(getContext());
            TextView textViewLugar           = new TextView(getContext());
            TextView textViewObjetivo        = new TextView(getContext());
            TextView textViewFechaInicio     = new TextView(getContext());
            TextView textViewFechaFin        = new TextView(getContext());
            TextView id                      = new TextView(getContext());

            //Colocamos la nombre a mostrar en la columna
            textViewNombre_docente.setText((fila.getnombre()));
            textViewActividad.setText((fila.getactividad()));
            textViewLugar.setText(fila.getlugar());
            textViewObjetivo.setText((fila.getobjetivo()));
            textViewFechaInicio.setText((fila.getfecha_inicio()));
            textViewFechaFin.setText((fila.getfecha_fin()));
            id.setText((fila.getid()));

            //Asignamos el color al text
            colortext(textViewNombre_docente);
            colortext(textViewActividad);
            colortext(textViewLugar);
            colortext(textViewObjetivo);
            colortext(textViewFechaInicio);
            colortext(textViewFechaFin);

            //*** COLUMNA NOMBRE Docente
            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreproductoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreproductoTableRowParams.setMargins(1, 1, 1, 1);
            nombreproductoTableRowParams.width = 246;
            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombre_docente, nombreproductoTableRowParams);

            TableRow.LayoutParams ActividadTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            ActividadTableRowParams.setMargins(1, 1, 1, 1);
            ActividadTableRowParams.width = 201;
            tableRow.addView(textViewActividad, ActividadTableRowParams);

            TableRow.LayoutParams lugarTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            lugarTableRowParams.setMargins(1, 1, 1, 1);
            lugarTableRowParams.width = 201;
            tableRow.addView(textViewLugar, lugarTableRowParams);

            TableRow.LayoutParams objetivoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            objetivoTableRowParams.setMargins(1, 1, 1, 1);
            objetivoTableRowParams.width = 201;
            tableRow.addView(textViewObjetivo, objetivoTableRowParams);

            TableRow.LayoutParams fechainicioTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            fechainicioTableRowParams.setMargins(1, 1, 1, 1);
            fechainicioTableRowParams.width = 126;
            tableRow.addView(textViewFechaInicio, fechainicioTableRowParams);

            TableRow.LayoutParams fechafinTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            fechafinTableRowParams.setMargins(1, 1, 1, 1);
            fechafinTableRowParams.width = 125;
            tableRow.addView(textViewFechaFin, fechafinTableRowParams);


            LinearLayout layout = new LinearLayout(getContext());
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


            final String s_id 	    = id.getText().toString().trim();

            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    //tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                    alertaEliminarEditar(s_id);
                }
            });
            //Agregamos la fila creada a la tabla de la pantalla
            mTableLayoutReporte.addView(tableRow);
        }
    }

    // metodo que se encargar de setiar el  color a los editex y el Size.
    public  void colortext( TextView text){

        text.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

        //Colocamos la orientacion del texto, tamaño, el estilo negritas y la altura del view
        text.setGravity(Gravity.CENTER);
        text.setTextSize(15);
        text.setTypeface(null, Typeface.BOLD);
        text.setHeight(50);
    }

    // VERIFICO LOS CAMPOS PARA PODER GUARDAR O EDITATR EL REGISTRO.
    public  void  VerificarCaposGuardarEditar(int accion){



        int respuesta_validar = validarCampos(accion_guardar);
        if(respuesta_validar==0){
            if(accion==accion_guardar){
                objGestionDB.insertatActividad(getContext(), id_docente, actividad, lugar, objetivo, fecha_inicio, fecha_fin);
                alertaConfirmacion("Actividad agregada Correctamente..");

            }else{
                // Toast.makeText(getContext(), "clickio "+i_id_actividad, Toast.LENGTH_SHORT).show();
                objGestionDB.actualizarActividad(getContext(), id_docente, actividad, lugar, objetivo, fecha_inicio, fecha_fin, i_id_actividad);
                alertaConfirmacion("Registro Actualizado Correctamente..");
            }
            setiarCampos();
        }else {
            alertaCompletarCampos("Complete los Campos");
        }
    }

    // click del boton buscar
    public void   clickBuscar(){

        int respuesta_validar = validarCampos(accion_buscar);
        if(respuesta_validar==0){
            // VERIFICO SI HAY REGISTROS EN LA BASE SEGUN LOS PARAMENTROS DE BUSQUEDA
            int cantidad_registro= objGestionDB.consultarRegistrosActividad(getContext(), filtro, actividad, lugar, objetivo, fecha_inicio, fecha_fin, id_docente, accion_buscar);
            if(cantidad_registro<1){
                tablavacia.setVisibility(View.VISIBLE);
                mTableLayoutReporte.removeAllViews();
                mTableLayoutReporte.removeAllViewsInLayout();
            }else{
                tablavacia.setVisibility(View.INVISIBLE);
                mListaActividades=objGestionDB.generarTablaActividad(getContext(), filtro, actividad,  lugar,  objetivo, fecha_inicio, fecha_fin, id_docente, accion_buscar);
                crearTabla();
            }
        }else {
            alertaCompletarCampos("Ingrese al menos un parametro para realizar la busqueda");
        }
    }


    public void  setiarCampos( ){

         //OCULTO
        editar.setVisibility(View.INVISIBLE);

        //Muestro
        guardar.setVisibility(View.VISIBLE);
        buscar.setVisibility(View.VISIBLE);

        textobuscar.setText("");
        e_actividad.setText("");
        e_lugar.setText("");
        e_objetivo.setText("");
        e_fecha_inicio.setText("");
        e_fecha_fin.setText("");

        filtro       = "";
        actividad    = "";
        lugar        = "";
        objetivo     = "";
        fecha_inicio = "";
        fecha_fin    = "";

        id_docente=0;

        loadListaDocente();
        CrearConsultaMostrarTablaActividad();
    }

    /*
  VALIDO LOS CAMPOS SEGÚN LA ACCION GUARDAR EDITAR O BUSCAR.
   @PARAM  accion
  @RETURN  respuesta_validar 0 si todolos campos estan completados y 1 si no estan todos completados CUANDO ACCION== GUARDAR O EDITAR
   */
    public  int  validarCampos(int accion){
        int respuesta_validar=0;

        actividad    = e_actividad.getText().toString().trim();
        lugar        = e_lugar.getText().toString().trim();
        objetivo     = e_objetivo.getText().toString().trim();
        fecha_inicio = e_fecha_inicio.getText().toString().trim();
        fecha_fin    = e_fecha_fin.getText().toString().trim();
        filtro       = textobuscar.getText().toString().trim();


        if(accion==accion_guardar){

            if( (filtro.equals("")) ){
                respuesta_validar=1;
                textobuscar.setError("Este Campo es requerido");
                textobuscar.requestFocus();
            } if(actividad.equals("")){
                respuesta_validar=1;
                e_actividad.setError("Este Campo es requerido");
                e_actividad.requestFocus();

            } if(lugar.equals("")){
                respuesta_validar=1;
                e_lugar.setError("Este Campo es requerido");
                e_lugar.requestFocus();

            } if(objetivo.equals("")){
                respuesta_validar=1;
                e_objetivo.setError("Este Campo es requerido");
                e_objetivo.requestFocus();

            } if (id_docente==0){

                if( (filtro.equals("")) ){}
                     else{
                    id_docente=objGestionDB.obtenerIdDocentePorNombre(getContext(),filtro);
                }
                if(id_docente==0){
                    respuesta_validar=1;
                    textobuscar.setError("Docente no existente");
                    textobuscar.requestFocus();
                }

            }  if(fecha_inicio.equals("")){
                respuesta_validar=1;
                e_fecha_inicio.setError("Este Campo es requerido");
                e_fecha_inicio.requestFocus();

            } if(fecha_fin.equals("")){
                respuesta_validar=1;
                e_fecha_fin.setError("Este Campo es requerido");
                e_fecha_fin.requestFocus();
            } if(fecha_inicio.equals("") ||fecha_fin.equals("") ){


            }else {
                boolean resultado=compararFechaFin(fecha_inicio,fecha_fin);
                if(resultado==false){
                    e_fecha_fin.setError("Fecha fin, es menor que Fecha Inicio ");
                    e_fecha_fin.requestFocus();
                    respuesta_validar=1;
                }
            }


        }//PRESIONO BUSCAR
        else if (accion==accion_buscar) {
            if(  (filtro.equals("")) && (actividad.equals("")) && (lugar.equals(""))  && (objetivo.equals("")) && (id_docente==0) && (fecha_inicio.equals("") && (fecha_fin.equals("")))  ) {

                respuesta_validar=1;
            }
        }
        return  respuesta_validar;
    }

    public void datePopup()
    {
        Time currDate = new Time(Time.getCurrentTimezone());
        currDate.setToNow();
        DatePickerDialog d = new DatePickerDialog(getContext(), datePickerListener,
                currDate.year,  currDate.month, currDate.monthDay);
        d.show();
    }


   private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,int selectedMonth, int selectedDay) {

           String fechita = selectedYear+"-"+(selectedMonth + 1)+"-"+selectedDay;

            if(b_fecha==b_fecha_inicio){
                  e_fecha_inicio.setText(fechita);
                 fecha_inicio   = e_fecha_inicio.getText().toString().trim();

            }else if(b_fecha=b_fecha_fin){
                   e_fecha_fin.setText(fechita);
                   fecha_fin      = e_fecha_fin.getText().toString().trim();

                   if(fecha_inicio.equals("")){
                     //no pasa nada.
                   }else {
                           boolean resultado=compararFechaFin(fecha_inicio,fecha_fin);
                           if(resultado==false){
                               e_fecha_fin.setError("Fecha fin, es menor que Fecha Inicio ");
                               e_fecha_fin.requestFocus();
                           }
                   }
            }
         }
    };




    public  Boolean  compararFechaFin(String fecha_inicio, String fecha_fin){
        boolean resultado=false;

        try{
            SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyy");
            Date datefecha_inicio =formateador.parse(fecha_inicio);
            Date datefecha_fin =formateador.parse(fecha_fin);
            Log.i("fecha fin","fecha fin"+datefecha_inicio+"  " +datefecha_fin);
            if(datefecha_inicio.before(datefecha_fin)){
                //fecha_inicio es menor
                resultado=true;

            }else if (datefecha_fin.before(datefecha_inicio)){
               // fecha_fin  es menor
                resultado =false;

               // Log.i("fecha fin","fecha fin"+resultado);
            }else {
              // las fechas son iguales ";
                resultado=true;
            }


        }catch (ParseException e){
            resultado=true;
           // Log.i("fecha fin","fecha fin malo"+fecha_inicio+"  "+fecha_fin);
        }

        return  resultado;
    }

    /*
   Cargo la lista docente
    */
    private void loadListaDocente()

    {
        List<SpinnerObject> lista =  objGestionDB.listaDocente(getContext(), filtro);

        ArrayAdapter<SpinnerObject> adap = new ArrayAdapter<SpinnerObject>(getContext(), R.layout.simplerow, lista);
        adap.setDropDownViewResource(R.layout.simplerow);
        listaDocente.setAdapter(adap);
    }

    /*
    BUSCO DE LA LISTA DOCENTE UN DOCENTE SEGUN EL FILTRO DIGITADO.
     */
    public  void buscarDocenteFiltro(){

        textobuscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (textobuscar.length() != 0) {
                    filtro = " LIKE '%" + textobuscar.getText().toString() + "%' ";
                    loadListaDocente();
                }
            }
        });

    }

    /*
    OBTENGO EL ID DEL ITEM SELECCIONADO DE LA LISTA DOCENTE
     */
    public  void obtenerIdLista(){

        listaDocente.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> elemento, View v, int position, long id) {
                id_docente = ((SpinnerObject) elemento.getItemAtPosition(position)).getId();
                String nombre = ((SpinnerObject) elemento.getItemAtPosition(position)).getValue();
                textobuscar.setText(nombre);
            }
        });
    }

    //ALERT COMPLETAR CAMPOS
    public void alertaCompletarCampos(final String mensaje)
    {

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.error);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloError);
        titulo.setText("Información!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeError);
        contenido.setText(mensaje);

        ((Button) customDialog.findViewById(R.id.btn_aceptarError)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }


    //ALERT CONFIRMACIÓN YA SEA PARA INSERT, DELETE O UPDATE!
    public void alertaConfirmacion(String mensaje)
    {

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("Confirmación!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText(mensaje);

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                setiarCampos();

            }
        });
        customDialog.show();
    }

    // ALERTA QUE PREGUNTA QUE HACER SI ELIMINAR O EDITAR EL REGISTRO SELECCIONADO DE LA TABLA.
    public void alertaEliminarEditar( final String id_actividad)
    {
        i_id_actividad = Integer.parseInt(id_actividad);
        // Toast.makeText(getContext(),"seleccionado"+id_docente, Toast.LENGTH_SHORT).show();
        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.modificar_elimar);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloModificarEliminar);
        titulo.setText("Que desea hacer");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeModificarElimar);
        contenido.setText("¿Modificar o Eliminar?");

        ((ImageView) customDialog.findViewById(R.id.img_eliminar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                alertaEliminarAceptarCancelar(id_actividad);
            }
        });

        ((ImageView) customDialog.findViewById(R.id.img_editar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                accion = accion_editar;
                setiarCamposEditar(accion, id_actividad);

            }
        });

        ((ImageView) customDialog.findViewById(R.id.ima_cerrar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();

            }
        });
        customDialog.show();
    }


    // ALERTA MULTIPLE DE ACEPTAR O CANCELAR LA ACCION DE ELIMINAR.
    public void alertaEliminarAceptarCancelar(final String id_actividad){

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.alerta);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloalerta);
        titulo.setText("Eliminar Actividad");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajealerta);
        contenido.setText("Una vez que elimines el registro, no podras deshacer la acción");

        ((Button) customDialog.findViewById(R.id.btn_cancelaraceptarAlerta)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                customDialog.dismiss();
            }
        });

        ((Button) customDialog.findViewById(R.id.btn_aceptarAlerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                objGestionDB.eliminarActividad(getContext(), id_actividad);
                customDialog.dismiss();
                alertaConfirmacion("Actividad Eliminada correctamente..");
            }
        });

        customDialog.show();
    }


    // SETIAR LOS CAMPOS, DEPENDIENDO SI FUE GUARDAR O SI VA EDITAR.
    public void  setiarCamposEditar(int accion, String id_actividad){


        if(accion==accion_editar){

            Log.i("editar","editar"+accion+" "+id_actividad);
            //OCULTO
            guardar.setVisibility(View.INVISIBLE);
            buscar.setVisibility(View.INVISIBLE);
            //MUESTRO
            editar.setVisibility(View.VISIBLE);
            cargar.setVisibility(View.VISIBLE);

            objGestionDB.cargarVariableActividadEditar(getContext(), id_actividad);
            textobuscar.setText(objGestionDB.nombre_docentecompleto);
            e_actividad.setText(objGestionDB.nombre_actividad);
            e_lugar.setText(objGestionDB.lugar_actividad);
            e_objetivo.setText(objGestionDB.objetivo_actividad);
            e_fecha_inicio.setText(objGestionDB.fecha_inicio);
            e_fecha_fin.setText(objGestionDB.fecha_fin);



            // cargarSpinnerFacultad();


        }


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
