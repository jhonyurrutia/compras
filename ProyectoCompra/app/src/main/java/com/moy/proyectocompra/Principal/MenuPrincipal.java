package com.moy.proyectocompra.Principal;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.moy.proyectocompra.Actividad.Actividad;
import com.moy.proyectocompra.CatedraDocente.CatedraDocente;

import com.moy.proyectocompra.Detalle.DetalleActividad;
import com.moy.proyectocompra.Docente.Docente;
import com.moy.proyectocompra.Login;
import com.moy.proyectocompra.R;

public class MenuPrincipal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
                   Docente.OnFragmentInteractionListener,
                   CatedraDocente.OnFragmentInteractionListener,
                   Actividad.OnFragmentInteractionListener,
                   DetalleActividad.OnFragmentInteractionListener{


    Dialog   customDialog=null;

    private static final String PREFRENCES_NAME = "sesionesSharedPreferences";
    int codigo_perfil     = 0;
    String nombre_usuario = "";
    TextView t_nombre_usuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

             inicializarControles();
    }


    public  void inicializarControles(){

        sesionSharedPreferences();
        t_nombre_usuario = (TextView)findViewById(R.id.Menu_principal_txt_nombre_usuario);
        t_nombre_usuario.setText(nombre_usuario);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }


    public void sesionSharedPreferences(){

        SharedPreferences preferencias = getSharedPreferences(PREFRENCES_NAME, Context.MODE_PRIVATE);
        nombre_usuario       = preferencias.getString("nombreusuario_sp", "");
        codigo_perfil        = preferencias.getInt("codigo_perfil", 0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/

       if(id == R.id.action_acerca_de){
           acercaDe();
           return true;
       }

      return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();
        android.support.v4.app.Fragment f=null;
        boolean fragmentSeleccionado=false;

        if (id == R.id.nav_docente) {

            if(codigo_perfil==2){
                sinPermisos();
            }else{

                f=new Docente();
                fragmentSeleccionado=true;
                // finish();
            }


        } else if (id == R.id.nav_catedra_docente) {
            if(codigo_perfil==2){
                sinPermisos();
            }else {

                f=new CatedraDocente();
                fragmentSeleccionado=true;
               // finish();
            }

        } else if (id == R.id.nav_actividad) {

            f=new Actividad();
            fragmentSeleccionado=true;

        } else if (id == R.id.nav_detalle) {
            f=new DetalleActividad();
            fragmentSeleccionado=true;

        } else if (id == R.id.nav_solicitud) {

        }
         else if (id == R.id.nav_home) {

            Intent i= new Intent(this, MenuPrincipal.class);
            startActivity(i);
           // finish();
         }
        else if (id == R.id.nav_cerrarsesion) {
            clikCerrarSesion();
           // finish();
        }
        if(fragmentSeleccionado)
        {
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedor_principal,f).commit();
            item.setChecked(true);
            getSupportActionBar().setTitle(item.getTitle());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    public void  acercaDe(){



        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.acerca_de);

        TextView titulo      = (TextView) customDialog.findViewById(R.id.acercaDe_textview_titulo);
        TextView version     = (TextView) customDialog.findViewById(R.id.acercaDe_textview_version);
        TextView ciclo = (TextView) customDialog.findViewById(R.id.acercaDe_textview_cilo);

        titulo.setText("CompraMaterialesApp");
        version.setText("Versión 1.0");
        ciclo.setText("");

        customDialog.show();
    }

    public void clikCerrarSesion() {

        SharedPreferences preferencias  = getSharedPreferences(PREFRENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();

        editor.putString("nombreusuario_sp", "");
        editor.putInt("codigo_perfil", 0);

        editor.commit();

        startActivity(new Intent(getBaseContext(), Login.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
    }


    public void sinPermisos()
    {

        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.permisos);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_titulopermiso);
        titulo.setText("Infomación");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajepermiso);
        contenido.setText("No posee permisos para realizar la función. Consulte el administrador. ");

        ((Button) customDialog.findViewById(R.id.btn_aceptarPermiso)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }


}
