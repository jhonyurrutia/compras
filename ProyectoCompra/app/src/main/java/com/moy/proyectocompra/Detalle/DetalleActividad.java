package com.moy.proyectocompra.Detalle;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.moy.proyectocompra.Actividad.TablaActividad;
import com.moy.proyectocompra.Principal.SpinnerObject;
import com.moy.proyectocompra.R;

import java.util.ArrayList;
import java.util.List;

import db_gestion.GestionDB;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetalleActividad.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetalleActividad#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetalleActividad extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    TextView t_nombre_actividad;
    EditText textobuscar;
    EditText e_cantidad,e_precio, e_caracteristicas, e_proveedor, e_descripcion,e_total;
    LinearLayout detalle;
    String filtro       = "";
    ListView listaActvidad;
    public GestionDB objGestionDB;
    Dialog customDialog=null;
    private TableLayout mTableLayoutReporte;
    private ArrayList<TablaDetalleActividad> mListaActividades;
    LinearLayout tablavacia;
    ImageView guardar, buscar, editar, cargar;

    int id_actividad   = 0;
    int  id_solicitud  = 0;
    int accion         = 0;
    int accion_guardar = 1;
    int accion_editar  = 2;
    int accion_buscar  = 3;

    String cantidad = "";
    String precio = "";
    String nombre_actividad="";
    String caracteristicas, proveedor, descripcion, total;

    Double d_cantidad, d_precio, d_total;


    private OnFragmentInteractionListener mListener;

    public DetalleActividad() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetalleActividad.
     */
    // TODO: Rename and change types and number of parameters
    public static DetalleActividad newInstance(String param1, String param2) {
        DetalleActividad fragment = new DetalleActividad();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        objGestionDB = new GestionDB(); // creo objeto gestion.
        //Inflamos el gragmennto y creamo el view para poder acceder a los controles del fragmento
        View view = inflater.inflate(R.layout.fragment_detalle_actividad, container, false);
        inicializarControles(view);
        loadListaActividad();
        buscarActividadFiltro();
        obtenerIdLista();
        clickImagenes();

        return view;
    }


    public  void inicializarControles(View view){

        textobuscar          = (EditText)view.findViewById(R.id.Detalle_Actividad_editText_Actividad);
        listaActvidad        = (ListView)view.findViewById(R.id.Detalle_Actividad_ListView_actividad);
        detalle              = (LinearLayout)view.findViewById(R.id.Detalle_actividad_btn_linear_detalle);
        cargar               = (ImageView)view.findViewById(R.id.Detalle_Actividad_imageView_cancelarCargar);


        /*textobuscar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Toast.makeText(getContext(), "tiene focus", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getContext(), "pierde focus", Toast.LENGTH_LONG).show();
                }
            }
        });*/

    }

         public  void clickImagenes(){

             //CLICK BNT DETALLE
             detalle.setOnClickListener(new View.OnClickListener() {
                 public void onClick(View v) {
                     clickDetalle();
                 }
             });

             //CLICK BNT RECARGAR
             cargar.setOnClickListener(new View.OnClickListener() {
                 public void onClick(View v) {
                     textobuscar.setText("");
                     filtro = "";
                     id_actividad = 0;
                     loadListaActividad();

                 }
             });

         }


    /*
    BUSCO DE LA LISTA DOCENTE UN DOCENTE SEGUN EL FILTRO DIGITADO.
     */
    public  void buscarActividadFiltro(){

        textobuscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (textobuscar.length() != 0) {
                    filtro = " LIKE '%" + textobuscar.getText().toString() + "%' ";
                    loadListaActividad();
                }
            }
        });

    }

    /*
   OBTENGO EL ID DEL ITEM SELECCIONADO DE LA LISTA ACTIVIDAD
    */
    public  void obtenerIdLista(){

        listaActvidad.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> elemento, View v, int position, long id) {
                id_actividad = ((SpinnerObject) elemento.getItemAtPosition(position)).getId();
                nombre_actividad = ((SpinnerObject) elemento.getItemAtPosition(position)).getValue();
                textobuscar.setText(nombre_actividad);
            }
        });
    }

    public  void  clickDetalle(){

        //Toast.makeText(getContext(), "presiono detalle"+id_actividad, Toast.LENGTH_SHORT).show();

        if(id_actividad==0){

        }else {
            alertaDetalleActividad();
        }
    }
    /*
   Cargo la lista de actividades
    */
    private void loadListaActividad()

    {
        List<SpinnerObject> lista =  objGestionDB.listaActividad(getContext(), filtro);

        ArrayAdapter<SpinnerObject> adap = new ArrayAdapter<SpinnerObject>(getContext(), R.layout.simplerow, lista);
        adap.setDropDownViewResource(R.layout.simplerow);
        listaActvidad.setAdapter(adap);
    }


    //ALERT DETALLE DE LA CTIVIDAD
    public void alertaDetalleActividad()
    {

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.alerta_detalle_actividad);

        String fecha_actual = objGestionDB.consultarfecha(getContext());

         objGestionDB.insertatSolicitud(getContext(),id_actividad,fecha_actual);
        incializarControlesAlert(customDialog);
        CrearConsultaMostrarTablaADetallectividad();




        e_precio.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Toast.makeText(getContext(), "tiene focus", Toast.LENGTH_LONG).show();
                } else {
                    cantidad = e_cantidad.getText().toString().trim();
                    precio = e_precio.getText().toString().trim();

                    if (cantidad.equals("") && precio.equals("")) {
                        Toast.makeText(getContext(), "pierde focus VACIO", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getContext(), "pierde focus LLENO", Toast.LENGTH_LONG).show();


                        d_cantidad = Double.parseDouble(cantidad);
                        d_precio = Double.parseDouble(precio);

                        e_total.setText("" + (d_cantidad * d_precio));

                    }
                }
            }
        });

        ((LinearLayout) customDialog.findViewById(R.id.Detalle_actividad_btn_linear_detalle)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //customDialog.dismiss();
                guardarDetalle();

            }
        });

        ((LinearLayout) customDialog.findViewById(R.id.Detalle_actividad_btn_linear_Candelar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //customDialog.dismiss();
                alertaEliminarAceptarCancelar();


            }
        });

        ((LinearLayout) customDialog.findViewById(R.id.Detalle_actividad_btn_linear_Aceptar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //customDialog.dismiss();
              objGestionDB.insertarTodosLosTetalles(getContext());


            }
        });
        ((ImageView) customDialog.findViewById(R.id.Detalle_Actividad_imageView_cancelarCargar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setiarCampos();

            }
        });
        customDialog.show();
    }

    public  void setiarCampos(){

        cantidad        = "";
        precio          = "";
        caracteristicas = "";
        proveedor       = "";
        descripcion     = "";
        total           = "";

        t_nombre_actividad.setText("");
        e_cantidad.setText("");
        e_precio.setText("");
        e_caracteristicas.setText("");
        e_proveedor.setText("");
        e_descripcion.setText("");
        e_total.setText("");

        CrearConsultaMostrarTablaADetallectividad();

    }

    public  void incializarControlesAlert(Dialog customDialog){

        t_nombre_actividad  = (TextView) customDialog.findViewById(R.id.Alerta_Detalle_Actividad_textview_nombre_actividad);
        e_cantidad          = (EditText) customDialog.findViewById(R.id.Detalle_actividad_Editext_cantidad);
        e_precio            = (EditText) customDialog.findViewById(R.id.Detalle_actividad_Editext_precio);
        e_caracteristicas   = (EditText) customDialog.findViewById(R.id.Detalle_actividad_Editext_caracteristica);
        e_proveedor         = (EditText) customDialog.findViewById(R.id.Detalle_actividad_Editext_proveedor);
        e_descripcion       = (EditText) customDialog.findViewById(R.id.Detalle_actividad_Editext_descripcion);
        e_total             = (EditText) customDialog.findViewById(R.id.Detalle_actividad_Editext_total);
        mTableLayoutReporte = (TableLayout)customDialog.findViewById(R.id.tablelayout_DetalleActividad);
        tablavacia          = (LinearLayout)customDialog.findViewById(R.id.Detalle_Actividad_linerlayout_tablavacia);

        t_nombre_actividad.setText(nombre_actividad);

    }


    /*
      MANDO A LLAMAR LE METODO EN LA GESTIONDB QUE SE ENCARGAR DE RETORNAR UN ARRAY para llenar mi tabla detalle actividad
    */
    public void CrearConsultaMostrarTablaADetallectividad(){
        //verifico si hay registros en la base
        int cantidad_registro= objGestionDB.consultarRegistrosDetalleActividad(getContext());
       // Log.i("cantidad_registro", "cantidad_registro" + cantidad_registro);
        if(cantidad_registro<1){
            tablavacia.setVisibility(View.VISIBLE);
            mTableLayoutReporte.removeAllViews();
            mTableLayoutReporte.removeAllViewsInLayout();

        } else {

            tablavacia.setVisibility(View.INVISIBLE);
            mListaActividades = objGestionDB.generarTablaDetalleActividad(getContext());
            crearTabla();
        }
    }


    // CREO LA TABLA Y LA PERSONALIZO PARA MOSTRAR LOS DATOS QUE ME INTERESAN.
    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion

        for (TablaDetalleActividad fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(getContext());

            //Creamos el texview donde se mostrara la informacion
            TextView textViewcantidad      = new TextView(getContext());
            TextView textViewprecio        = new TextView(getContext());
            TextView textViewcaracteristia = new TextView(getContext());
            TextView textViewproveedor     = new TextView(getContext());
            TextView textViewdescripcion   = new TextView(getContext());
            TextView textViewtotal         = new TextView(getContext());
            TextView id                    = new TextView(getContext());

            //Colocamos la nombre a mostrar en la columna
            textViewcantidad.setText((fila.getcantidad()));
            textViewprecio.setText("$ "+(fila.getprecio()));
            textViewcaracteristia.setText(fila.getcaracteristicas());
            textViewproveedor.setText((fila.getproeveedor()));
            textViewdescripcion.setText((fila.getdescripcion()));
            textViewtotal.setText("$ "+(fila.gettotal()));
            id.setText((fila.getid()));

            //Asignamos el color al text
            colortext(textViewcantidad);
            colortext(textViewprecio);
            colortext(textViewcaracteristia);
            colortext(textViewproveedor);
            colortext(textViewdescripcion);
            colortext(textViewtotal);


            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams cantidadproductoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            cantidadproductoTableRowParams.setMargins(1, 1, 1, 1);
            cantidadproductoTableRowParams.width = 96;
            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewcantidad, cantidadproductoTableRowParams);

            TableRow.LayoutParams precioTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            precioTableRowParams.setMargins(1, 1, 1, 1);
            precioTableRowParams.width = 101;
            tableRow.addView(textViewprecio, precioTableRowParams);

            TableRow.LayoutParams caracteristicasTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            caracteristicasTableRowParams.setMargins(1, 1, 1, 1);
            caracteristicasTableRowParams.width = 201;
            tableRow.addView(textViewcaracteristia, caracteristicasTableRowParams);

            TableRow.LayoutParams proveedorTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            proveedorTableRowParams.setMargins(1, 1, 1, 1);
            proveedorTableRowParams.width = 201;
            tableRow.addView(textViewproveedor, proveedorTableRowParams);

            TableRow.LayoutParams descripcionTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            descripcionTableRowParams.setMargins(1, 1, 1, 1);
            descripcionTableRowParams.width = 201;
            tableRow.addView(textViewdescripcion, descripcionTableRowParams);

            TableRow.LayoutParams totalTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            totalTableRowParams.setMargins(1, 1, 1, 1);
            totalTableRowParams.width = 100;
            tableRow.addView(textViewtotal, totalTableRowParams);


            LinearLayout layout = new LinearLayout(getContext());
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


            final String s_id 	    = id.getText().toString().trim();

            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    //tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
                    //alertaEliminarEditar(s_id);
                }
            });
            //Agregamos la fila creada a la tabla de la pantalla
            mTableLayoutReporte.addView(tableRow);
        }
    }

    // metodo que se encargar de setiar el  color a los editex y el Size.
    public  void colortext( TextView text){

        text.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

        //Colocamos la orientacion del texto, tamaño, el estilo negritas y la altura del view
        text.setGravity(Gravity.CENTER);
        text.setTextSize(15);
        text.setTypeface(null, Typeface.BOLD);
        text.setHeight(50);
    }

    public  void guardarDetalle(){

        int respuesta=validarCamposAlert();
        if(respuesta==0){
            objGestionDB.insertatDetalleActividad(getContext(), cantidad, precio, caracteristicas, proveedor, descripcion, total,id_solicitud);
            alertaConfirmacion("Detalle agregado Correctamente..");
        }else{
            alertaCompletarCampos("Complete los Campos");
        }
    }

    public  int validarCamposAlert(){

        cantidad        = e_cantidad.getText().toString().trim();
        precio          = e_precio.getText().toString().trim();
        caracteristicas = e_caracteristicas.getText().toString().trim();
        proveedor       = e_proveedor.getText().toString().trim();
        descripcion     = e_descripcion.getText().toString().trim();
        total           = e_total.getText().toString().trim();

        int respuesta=0;
        if(cantidad.equals("") || precio.equals("") || caracteristicas.equals("") || proveedor.equals("") || descripcion.equals("") || total.equals("") ){
            respuesta=1;
        }
        return respuesta;
    }

    //ALERT COMPLETAR CAMPOS
    public void alertaCompletarCampos(final String mensaje)
    {

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.error);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloError);
        titulo.setText("Información!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeError);
        contenido.setText(mensaje);

        ((Button) customDialog.findViewById(R.id.btn_aceptarError)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }


    //ALERT CONFIRMACIÓN YA SEA PARA INSERT, DELETE O UPDATE!
    public void alertaConfirmacion(String mensaje)
    {

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("Confirmación!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText(mensaje);

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                setiarCampos();

            }
        });
        customDialog.show();
    }


    // ALERTA MULTIPLE DE ACEPTAR O CANCELAR LA ACCION DE ELIMINAR.
    public void alertaEliminarAceptarCancelar(){

        customDialog = new Dialog(getContext(),R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.alerta);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloalerta);
        titulo.setText("Cancelar Detalle de Actividad");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajealerta);
        contenido.setText("Una vez que elimines los registro, no podras deshacer la acción");

        ((Button) customDialog.findViewById(R.id.btn_cancelaraceptarAlerta)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                customDialog.dismiss();
            }
        });

        ((Button) customDialog.findViewById(R.id.btn_aceptarAlerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                objGestionDB.eliminarSolicitud(getContext(), id_solicitud);
                objGestionDB.eliminarDetalle(getContext(), id_solicitud);
                customDialog.dismiss();
                alertaConfirmacion("Detalles Eliminados correctamente..");
            }
        });

        customDialog.show();
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
